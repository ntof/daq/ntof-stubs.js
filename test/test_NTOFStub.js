// @ts-check
const
  { invoke, keyBy } = require('lodash'),
  { NTOFStub } = require('../src'),
  { DicXmlDataSet } = require('@ntof/dim-xml'),
  { expect } = require('chai'),
  { afterEach, describe, it } = require('mocha');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('NTOFStub', function() {
  /** @type {{ stub?: NTOFStub, client?: DicXmlDataSet }} */
  let env = {};

  afterEach(function() {
    invoke(env.stub, 'close');
    invoke(env.client, 'release');
    env = {};
  });

  it('can create an NTOFStub', async function() {
    env.stub = new NTOFStub({
      daqs: [
        { crateId: 0, hostname: 'test' },
        { crateId: 1, layout: [ 2, 4 ] }
      ]
    });
    await env.stub.init();
  });

  it('creates an NTOFStub with highVoltage', async function() {
    env.stub = new NTOFStub({
      daqs: [ { crateId: 0, hostname: 'test' } ],
      hv: [ { cardId: 0, nChannels: 2 } ]
    });
    await env.stub.init();

    env.client = new DicXmlDataSet('HV_0/Acquisition', null, env.stub.dns.url());
    await env.client.promise();
    expect(keyBy(env.client.value, 'name'))
    .to.have.property('chan1caenErrorCode');

    env.client.release();
  });
});
