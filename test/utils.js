// @ts-check
const
  { makeDeferred, timeout } = require('@cern/prom'),
  debug = require('debug')("test:utils");

/**
 * @param {any} obj
 * @param {string} evt
 * @param {number | function(any): boolean} test
 * @param {number} tm
 * @returns {Promise<any>}
 */
async function waitEvent(obj, evt, test, tm = 1000) {

  const count = (typeof test === 'number') ? test : -1;
  let received = 0;
  const def = makeDeferred();
  const ret = [];

  const cb = function(e) {
    ret.push(e);
    debug('waitEvent: received:%s', evt);
    if (count > 0 && ++received >= count) {
      def.resolve(e);
    }
    // @ts-ignore checked above
    else if (test(e)) {
      def.resolve(e);
    }
  };
  obj.on(evt, cb);
  return timeout(def.promise, tm)
  .catch(() => {
    throw new Error("Event '" + evt + "' not received, got: " +
      JSON.stringify(ret, undefined, 2));
  })
  .finally(() => { obj.removeListener(evt, cb); });
}

module.exports = { waitEvent };
