// @ts-check
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlState, DicXmlParams, XmlData } =  require('@ntof/dim-xml'),
  { DaqStub } = require('../src'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { waitEvent } = require('./utils');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('DaqStub', function() {
  let dns;
  let node;
  /** @type DicXmlState | DicXmlParams */
  let client;
  /** @type DaqStub */
  let daq;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    if (node) {
      node.close();
      node = null;
    }
    if (client) {
      client.release();
      client = null;
    }
    if (daq) {
      daq.close();
      daq = null;
    }
    dns.close();
  });

  it('can create a Daq', async function() {
    node = new DisXmlNode(null, 0);

    daq = new DaqStub(dns.url());
    daq.register("mydaq", node);
    daq.run();
    await node.register(dns.url());

    client = new DicXmlState("mydaq/DaqState", null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.get(e, 'strValue') === "Not configured");
  });

  it('can configure ZSP', async function() {
    node = new DisXmlNode(null, 0);

    daq = new DaqStub(dns.url());
    daq.register("mydaq", node);
    daq.run();
    await node.register(dns.url());

    client = new DicXmlParams("mydaq/ZeroSuppression", null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.get(_.find(e, { index: 1 }), 'valueName') === 'independent');

    await client.setParams([
      { index: 1, type: XmlData.Type.STRING, value: "singlemaster" },
      { index: 2, value: [
        { index: 0, value: [
          { index: 0, type: XmlData.Type.STRING, value: 'SPD-02913' },
          { index: 1, type: XmlData.Type.UINT32, value: 0 },
          { index: 2, value: [
            { index: 0, type: XmlData.Type.STRING, value: 'SPD-02913' },
            { index: 1, type: XmlData.Type.UINT32, value: 1 }
          ] }
        ] }
      ] }
    ]);

    await client.setParams([
      { index: 1, type: XmlData.Type.STRING, value: "singlemaster" },
      { index: 2, value: [
        { index: 0, value: [
          { index: 0, type: XmlData.Type.STRING, value: 'wrong' },
          { index: 1, type: XmlData.Type.UINT32, value: 0 }
        ] }
      ] }
    ])
    .then(() => { throw 'should fail'; }, _.noop);

    expect(_.get(daq.zspService, 'data[2].value')).to.deep.equal([
      { index: 0, name: "master", value: [
        { index: 0, name: "sn", type: XmlData.Type.STRING, value: 'SPD-02913', unit: '' },
        { index: 1, name: "channel", type: XmlData.Type.UINT32, value: 0, unit: '' },
        { index: 2, name: 'slave', value: [
          { index: 0, name: 'sn', type: XmlData.Type.STRING, value: 'SPD-02913', unit: '' },
          { index: 1, name: 'channel', type: XmlData.Type.UINT32, value: 1, unit: '' }
        ] }
      ] }
    ]);
  });
});
