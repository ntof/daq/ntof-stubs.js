// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlState } =  require('@ntof/dim-xml'),
  { RFMergerStub } = require('../src'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { waitEvent } = require('./utils');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('RFMergerStub', function() {
  let dns;
  let node;
  /** @type DicXmlState */
  let client;
  /** @type RFMergerStub */
  let rfmerger;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    if (node) {
      node.close();
      node = null;
    }
    if (client) {
      client.release();
      client = null;
    }
    if (rfmerger) {
      rfmerger.close();
      rfmerger = null;
    }
    dns.close();
  });

  it('can create a RFMerger', async function() {
    node = new DisXmlNode(null, 0);

    rfmerger = new RFMergerStub();
    rfmerger.register("MERGER", node);
    await node.register(dns.url());

    client = new DicXmlState("MERGER/State", null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.get(e, 'strValue') === "OK");
  });
});
