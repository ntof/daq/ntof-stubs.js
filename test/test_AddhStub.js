// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlValue, xml } =  require('@ntof/dim-xml'),
  { AddhStub } = require('../src'),
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('AddhStub', function() {
  let dns;
  let node;
  /** @type DicXmlValue */
  let client;
  /** @type AddhStub */
  let addh;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    _.invoke(node, 'close');
    _.invoke(client, 'release');
    // @ts-ignore
    node = client = alarmserver = null;
    dns.close();
  });

  it('can create an Addh Stub', async function() {
    node = new DisXmlNode(null, 0);

    addh = new AddhStub();
    addh.register(node);
    await node.register(dns.url());

    client = new DicXmlValue('ADDH/Extra', null, dns.url());
    const info = /** @type Element */(await client.promise());
    const infoJs = xml.toJs(info);
    const dataArray = _.get(infoJs, 'data');
    const valueArray = _.get(infoJs, 'value');
    expect(dataArray).not.undefined();
    expect(dataArray.length).to.be.above(0);
    expect(valueArray).not.undefined();
    expect(valueArray.length).to.be.above(0);
  });
});
