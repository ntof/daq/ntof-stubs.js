// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlParams, DicXmlValue, XmlData, xml } =  require('@ntof/dim-xml'),
  { TimingStub } = require('../src'),
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { waitEvent } = require('./utils');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('TimingStub', function() {
  let dns;
  let node;
  /** @type DicXmlParams | DicXmlValue | null */
  let client;
  /** @type TimingStub */
  let timing;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    if (node) {
      node.close();
      node = null;
    }
    if (client) {
      client.release();
      client = null;
    }
    if (timing) {
      timing.close();
      timing = null;
    }
    dns.close();
  });

  it('can create a Timing machine', async function() {
    node = new DisXmlNode(null, 0);

    timing = new TimingStub();
    timing.register(node);
    await node.register(dns.url());

    client = new DicXmlParams('Timing', null, dns.url());
    const info = /** @type XmlData[] */(await client.promise());
    expect(_.find(info, { name: 'mode' }))
    .to.deep.contain({ valueName: "DISABLED" });
  });

  it('can tick a Timing machine', async function() {
    node = new DisXmlNode(null, 0);

    timing = new TimingStub();
    timing.register(node);
    await node.register(dns.url());

    client = new DicXmlParams('Timing', null, dns.url());
    await client.promise();
    await client.setParams([
      { index: TimingStub.ParamIdx.MODE, value: TimingStub.Mode.CALIBRATION, type: XmlData.Type.ENUM },
      { index: TimingStub.ParamIdx.EVENT_NUMBER, value: 0, type: XmlData.Type.INT64 },
      { index: TimingStub.ParamIdx.PERIOD, value: 100, type: XmlData.Type.INT32 }
    ]);

    client.release();
    client = new DicXmlValue('Timing/event', null, dns.url());
    await waitEvent(client, 'value',
      (e) => e && _.get(xml.toJs(e), 'event.$.name') === 'CALIBRATION');
  });
});
