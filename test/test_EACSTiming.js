// @ts-check
const
  { assign, invoke } = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode } =  require('@ntof/dim-xml'),
  { TimingStub } = require('../src'),
  EACSTimingStub = require('../src/EACS/EACSTiming'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { expect } = require('chai');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('EACSTiming', function() {
  let dns;
  let node;
  /** @type EACSTimingStub */
  let stub;
  /** @type TimingStub */
  let timing;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    invoke(node, 'close');
    invoke(stub, 'release');
    invoke(timing, 'close');
    node = stub = timing = null;
    dns.close();
  });

  it('can control Timing', async function() {
    node = new DisXmlNode(null, 0);

    timing = new TimingStub();
    timing.register(node);
    // @ts-ignore it's ok
    timing.update([
      assign({ value: 42 }, TimingStub.Params.triggerPause),
      assign({ value: TimingStub.Mode.AUTOMATIC }, TimingStub.Params.mode)
    ]);
    await node.register(dns.url());
    // do this first, otherwise first request may fail du to not-yet-existing node

    stub = new EACSTimingStub(dns.url());
    node.addXmlParams('EACS/Timing', stub);

    await stub.syncOnce();
    expect(stub.get(TimingStub.Params.triggerPause.index)).to.equal(42);
    expect(stub.get(TimingStub.Params.mode.index))
    .to.equal(TimingStub.Mode.AUTOMATIC);

    await stub.suspend();
    expect(timing.get(TimingStub.Params.mode.index))
    .to.equal(TimingStub.Mode.DISABLED);

    // @ts-ignore it's ok
    stub.update([
      assign({ value: TimingStub.Mode.CALIBRATION }, TimingStub.Params.mode)
    ]);

    await stub.start();
    expect(timing.get(TimingStub.Params.mode.index))
    .to.equal(TimingStub.Mode.CALIBRATION);
  });
});
