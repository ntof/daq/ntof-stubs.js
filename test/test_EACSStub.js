// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlState, DicXmlDataSet, DicXmlParams } =  require('@ntof/dim-xml'),
  { EACSStub, DaqStub, TimingStub } = require('../src'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { waitEvent } = require('./utils'),
  { expect } = require('chai');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('EACSStub', function() {
  let dns;
  let node;
  /** @type DicXmlState | DicXmlDataSet */
  let client;
  /** @type EACSStub */
  let eacs;
  /** @type Array<DaqStub> */
  let daqs = [];
  /** @type TimingStub */
  let timing;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    _.invoke(node, 'close');
    _.invoke(client, 'release');
    _.invoke(eacs, 'close');
    _.invoke(timing, 'close');
    node = client = eacs = timing = null;
    daqs.forEach((d) => d.close());
    daqs = [];
    dns.close();
  });

  it('can create an EACSStub', async function() {
    node = new DisXmlNode(null, 0);

    eacs = new EACSStub(null, dns.url());
    eacs.register("EACS", node);
    await node.register(dns.url());

    timing = new TimingStub();
    timing.register(node);

    client = new DicXmlState("EACS/State", null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.get(e, 'strValue') === "IDLE");
  });

  it('can list daqs', async function() {
    node = new DisXmlNode(null, 0);

    eacs = new EACSStub([ 'ntofdaq-m0', 'ntofdaq-m1' ], dns.url());
    eacs.register("EACS", node);
    await node.register(dns.url());

    var daq = new DaqStub(dns.url());
    daq.register('ntofdaq-m0', node);
    daqs.push(daq);

    daq = new DaqStub(dns.url());
    daq.register('ntofdaq-m1', node);
    daqs.push(daq);

    client = new DicXmlDataSet('EACS/Daq/List', null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.size(e) === 2);
    client.release();

    client = new DicXmlParams('EACS/Daq/ntofdaq-m1/CARD0/CHANNEL0', null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.get(e, '[2].value') === "BAF2");
  });

  it('mirrors zeroSuppression service', async function() {
    node = new DisXmlNode(null, 0);

    eacs = new EACSStub([ 'ntofdaq-m0' ], dns.url());
    eacs.register("EACS", node);
    await node.register(dns.url());

    var daq = new DaqStub(dns.url());
    daq.register('ntofdaq-m0', node);
    daqs.push(daq);

    client = new DicXmlParams('EACS/Daq/ntofdaq-m0/ZeroSuppression', null, dns.url());
    await waitEvent(client, 'value', (e) => _.size(e) === 3);
    client.release();
  });

  it('can get EACS info', async function() {
    node = new DisXmlNode(null, 0);

    eacs = new EACSStub([], dns.url());
    eacs.register("EACS", node);
    eacs.eacsInfo.addHighVoltage(0, 2);
    eacs.eacsInfo.addHighVoltage(1, 2);
    eacs.eacsInfo.addFilterStation("filterStation");
    await node.register(dns.url());

    client = new DicXmlDataSet('EACS/Info', null, dns.url());
    await waitEvent(client, 'value',
      (e) => _.find(e, { name: 'filterStation' }));

    const filterStation = _.find(client.value, { name: 'filterStation' });
    expect(filterStation.value).to.equal('filterStation');

    const hv = _.find(client.value, { name: 'highVoltage' });
    expect(hv).to.deep.include({ index: 1, name: "highVoltage" });
    expect(_.find(hv.value, { name: "HV_0" })).to.deep.include({
      value: [
        { index: 0, name: "channel", value: [] },
        { index: 1, name: "channel", value: [] }
      ]
    });
    client.release();
  });
});
