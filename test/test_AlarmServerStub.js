// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlValue, xml } =  require('@ntof/dim-xml'),
  { AlarmServerStub } = require('../src'),
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('AlarmServerStub', function() {
  let dns;
  let node;
  /** @type DicXmlValue */
  let client;
  /** @type AlarmServerStub */
  let alarmserver;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    _.invoke(node, 'close');
    _.invoke(client, 'release');
    _.invoke(alarmserver, 'close');
    node = client = alarmserver = null;
    dns.close();
  });

  it('can create an Alarm Server', async function() {
    node = new DisXmlNode(null, 0);

    alarmserver = new AlarmServerStub();
    alarmserver.register(node);
    await node.register(dns.url());

    client = new DicXmlValue('Alarms', null, dns.url());
    const info = /** @type Element */(await client.promise());
    const infoJs = xml.toJs(info);
    const alarmsArray = _.get(infoJs, 'alarms.alarm');
    expect(alarmsArray).not.undefined();
    expect(_.find(alarmsArray, { '$textContent': 'alarm message alarm1' }))
    .not.undefined();

    // Check stats and cmd
    const stats = _.get(infoJs, 'stats');

    // Count masked = 1
    expect(_.filter(alarmserver.alarms.alarms, { masked: true }).length).to.equal(1);
    expect(_.toNumber(_.get(stats, "$.masked"))).to.equal(1);
  });
});
