// @ts-check
const
  _ = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode, DicXmlValue, xml } =  require('@ntof/dim-xml'),
  { BeamLineStub } = require('../src'),
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha');

/**
 * @typedef {import('@cern/dim').DicValue} DicValue
 */

describe('BeamLineStub', function() {
  let dns;
  let node;
  /** @type DicXmlValue */
  let client;
  /** @type BeamLineStub */
  let beamline;

  beforeEach(async function() {
    dns = new DnsServer(null, 0);
    await dns.listen();
  });

  afterEach(function() {
    _.invoke(node, 'close');
    _.invoke(client, 'release');
    node = client = null;
    dns.close();
  });

  it('can create Beam Line Stub', async function() {
    node = new DisXmlNode(null, 0);

    beamline = new BeamLineStub(1, 1, 1);
    beamline.register(node);
    await node.register(dns.url());

    client = new DicXmlValue('TOF_VGR0.State', null, dns.url());
    const info = /** @type Element */(await client.promise());
    const infoJs = xml.toJs(info);
    const data = _.get(infoJs, 'data.$');
    expect(data).not.undefined();
    expect(data.name).is.equal('__DIP_DEFAULT__');
    expect(data.value).is.equal('0');
  });
});
