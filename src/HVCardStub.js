// @ts-check
const
  { DisXmlDataSet, XmlData } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:hv');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

const CHAN_PARAM_SIZE = 25;
const AcqParamIdx = {
  ACQ_STAMP: 0,
  CYCLE_STAMP: 1,
  CYCLE_NAME: 2,
  CHAN_ERROR_CODE: 5,
  CHAN_ERROR_MESSAGE: 6,
  CHAN_NAME: 7,
  CHAN_STATUS_BITFIELD: 8,
  CHAN_I0SET: 9,
  CHAN_I0SET_MAX: 10,
  CHAN_I0SET_MIN: 11,
  CHAN_IMON: 12,
  CHAN_PW: 13,
  CHAN_RDWN: 14,
  CHAN_RDWN_MAX: 15,
  CHAN_RDWN_MIN: 16,
  CHAN_RUP: 17,
  CHAN_RUP_MAX: 18,
  CHAN_RUP_MIN: 19,
  CHAN_SVMAX: 20,
  CHAN_SVMAX_MAX: 21,
  CHAN_SVMAX_MIN: 22,
  CHAN_TRIP: 23,
  CHAN_TRIP_MAX: 24,
  CHAN_TRIP_MIN: 25,
  CHAN_V0SET: 26,
  CHAN_V0SET_MAX: 27,
  CHAN_V0SET_MIN: 28,
  CHAN_VMON: 29
};

const BoardInfoParamIdx = {
  ACQ_STAMP: 0,
  CYCLE_STAMP: 1,
  CYCLE_NAME: 2,
  DESCRIPTION: 3,
  FIRMWARE: 4,
  MODEL: 5,
  SERIAL: 6
};

const PowerMode = {
  UNKNOWN: 0,
  ON: 1,
  OFF: 2
};

const getNowInNanoseconds = () => new Date().getTime() * 1000 * 1000;

class HVCardStub extends DisXmlDataSet {
  /**
   * @param {number} cardIndex
   * @param {number} nChannels
   */
  constructor(cardIndex, nChannels) {
    super();
    this.cardIndex = cardIndex;
    this.nChannels = nChannels;

    const timestamp = getNowInNanoseconds(); // Nanoseconds

    this.add({ name: "acqStamp", index: AcqParamIdx.ACQ_STAMP, unit: "",
      type: XmlData.Type.INT64, value: timestamp });
    this.add({ name: "cycleStamp", index: AcqParamIdx.CYCLE_STAMP, unit: "",
      type: XmlData.Type.INT64, value: timestamp });
    this.add({ name: "cycleName", index: AcqParamIdx.CYCLE_NAME, unit: "",
      type: XmlData.Type.STRING, value: "tof" });

    for (var channelIndex = 0; channelIndex < this.nChannels; channelIndex++) {
      this.addChannel(channelIndex, `channel_${channelIndex}`, true, 0.5, 15);
    }

    const slotIndex = 3 + (this.nChannels * CHAN_PARAM_SIZE) + 2;
    this.add({ name: "slot", index: slotIndex, unit: "",
      type: XmlData.Type.INT16, value: this.cardIndex });

    this.boardInfo = new DisXmlDataSet();
    this.boardInfo.add({ name: "acqStamp", index: BoardInfoParamIdx.ACQ_STAMP, unit: "",
      type: XmlData.Type.INT64, value: timestamp });
    this.boardInfo.add({ name: "cycleStamp", index: BoardInfoParamIdx.CYCLE_STAMP, unit: "",
      type: XmlData.Type.INT64, value: timestamp });
    this.boardInfo.add({ name: "cycleName", index: BoardInfoParamIdx.CYCLE_NAME, unit: "",
      type: XmlData.Type.STRING, value: "tof" });
    this.boardInfo.add({ name: "description", index: BoardInfoParamIdx.DESCRIPTION, unit: "",
      type: XmlData.Type.STRING, value: " 12 Ch Neg. 3KV 10uA " });
    this.boardInfo.add({ name: "firmware", index: BoardInfoParamIdx.FIRMWARE, unit: "",
      type: XmlData.Type.STRING, value: "3.0" });
    this.boardInfo.add({ name: "model", index: BoardInfoParamIdx.MODEL, unit: "",
      type: XmlData.Type.STRING, value: "A1821H" });
    this.boardInfo.add({ name: "serialNumber", index: BoardInfoParamIdx.SERIAL, unit: "",
      type: XmlData.Type.INT32, value: 841 });

    this._timer = setInterval(this.updateAcqStamp.bind(this), 1000);
  }

  updateAcqStamp() {
    const timestamp = getNowInNanoseconds();
    this.update(AcqParamIdx.ACQ_STAMP, timestamp);
    this.boardInfo.update(BoardInfoParamIdx.ACQ_STAMP, timestamp);
  }

  close() {
    clearInterval(this._timer);
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlDataSet('HV_' + this.cardIndex + '/Acquisition', this);
    node.addXmlDataSet('HV_' + this.cardIndex + '/BoardInformation', this.boardInfo);
    debug('stub registered for card: %d', this.cardIndex);
  }

  /**
   * @param {number} index
   * @param {string} name
   * @param {boolean} enabled
   * @param {number} iset
   * @param {number} vset
   */
  addChannel(index, name, enabled, iset, vset) {
    const prefix = 'chan' + index;
    const indexOffset = index * CHAN_PARAM_SIZE;

    this.add({ name: prefix + 'caenErrorCode',
      index: indexOffset + AcqParamIdx.CHAN_ERROR_CODE, type: XmlData.Type.INT32, value: 0 });
    this.add({ name: prefix + 'caenErrorMessage',
      index: indexOffset + AcqParamIdx.CHAN_ERROR_MESSAGE, type: XmlData.Type.STRING, value: '' });
    this.add({ name: prefix + 'channelName',
      index: indexOffset + AcqParamIdx.CHAN_NAME, type: XmlData.Type.STRING, value: name });
    this.add({ name: prefix + 'channelStatusBitfield',
      index: indexOffset + AcqParamIdx.CHAN_STATUS_BITFIELD, type: XmlData.Type.INT32, value: 0 });
    this.add({ name: prefix + 'i0set', index: indexOffset + AcqParamIdx.CHAN_I0SET,
      type: XmlData.Type.FLOAT, value: iset });
    this.add({ name: prefix + 'i0set_max', index: indexOffset + AcqParamIdx.CHAN_I0SET_MAX,
      type: XmlData.Type.FLOAT, value: iset + 1 });
    this.add({ name: prefix + 'i0set_min', index: indexOffset + AcqParamIdx.CHAN_I0SET_MIN,
      type: XmlData.Type.FLOAT, value: 0 });
    this.add({ name: prefix + 'imon', index: indexOffset + AcqParamIdx.CHAN_IMON,
      type: XmlData.Type.FLOAT, value: iset });
    this.add({ name: prefix + 'pw', index: indexOffset + AcqParamIdx.CHAN_PW,
      type: XmlData.Type.INT32, value: enabled ? PowerMode.ON : PowerMode.OFF });
    this.add({ name: prefix + 'rdwn', index: indexOffset + AcqParamIdx.CHAN_RDWN,
      type: XmlData.Type.FLOAT, value: 15 });
    this.add({ name: prefix + 'rdwn_max', index: indexOffset + AcqParamIdx.CHAN_RDWN_MAX,
      type: XmlData.Type.FLOAT, value: 500 });
    this.add({ name: prefix + 'rdwn_min', index: indexOffset + AcqParamIdx.CHAN_RDWN_MIN,
      type: XmlData.Type.FLOAT, value: 1 });
    this.add({ name: prefix + 'rup', index: indexOffset + AcqParamIdx.CHAN_RUP,
      type: XmlData.Type.FLOAT, value: 1 });
    this.add({ name: prefix + 'rup_max', index: indexOffset + AcqParamIdx.CHAN_RUP_MAX,
      type: XmlData.Type.FLOAT, value: 500 });
    this.add({ name: prefix + 'rup_min', index: indexOffset + AcqParamIdx.CHAN_RUP_MIN,
      type: XmlData.Type.FLOAT, value: 1 });
    this.add({ name: prefix + 'svmax', index: indexOffset + AcqParamIdx.CHAN_SVMAX,
      type: XmlData.Type.FLOAT, value: vset + 1 });
    this.add({ name: prefix + 'svmax_max', index: indexOffset + AcqParamIdx.CHAN_SVMAX_MAX,
      type: XmlData.Type.FLOAT, value: 3000 });
    this.add({ name: prefix + 'svmax_min', index: indexOffset + AcqParamIdx.CHAN_SVMAX_MIN,
      type: XmlData.Type.FLOAT, value: 0 });
    this.add({ name: prefix + 'trip', index: indexOffset + AcqParamIdx.CHAN_TRIP,
      type: XmlData.Type.FLOAT, value: 2 });
    this.add({ name: prefix + 'trip_max', index: indexOffset + AcqParamIdx.CHAN_TRIP_MAX,
      type: XmlData.Type.FLOAT, value: 1000 });
    this.add({ name: prefix + 'trip_min', index: indexOffset + AcqParamIdx.CHAN_TRIP_MIN,
      type: XmlData.Type.FLOAT, value: 0 });
    this.add({ name: prefix + 'v0set', index: indexOffset + AcqParamIdx.CHAN_V0SET,
      type: XmlData.Type.FLOAT, value: vset });
    this.add({ name: prefix + 'v0set_max', index: indexOffset + AcqParamIdx.CHAN_V0SET_MAX,
      type: XmlData.Type.FLOAT, value: vset + 1 });
    this.add({ name: prefix + 'v0set_min', index: indexOffset + AcqParamIdx.CHAN_V0SET_MIN,
      type: XmlData.Type.FLOAT, value: 0 });
    this.add({ name: prefix + 'vmon', index: indexOffset + AcqParamIdx.CHAN_VMON,
      type: XmlData.Type.FLOAT, value: vset });
    debug('channel added: %d on card %d', index, this.cardIndex);
  }
}
HVCardStub.AcqParams = AcqParamIdx;
HVCardStub.BoardInfoParams = BoardInfoParamIdx;
HVCardStub.PowerMode = PowerMode;

module.exports = HVCardStub;
