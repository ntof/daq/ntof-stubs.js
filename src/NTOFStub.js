// @ts-check
const
  debug = require('debug')('stub:ntof'),
  { invoke, forEach, get } = require('lodash'),
  { DnsServer } = require('@cern/dim'),
  { DisXmlNode } = require('@ntof/dim-xml'),
  RFMergerStub = require('./RFMerger/RFMergerStub'),
  TimingStub = require('./TimingStub'),
  DaqStub = require('./daq/DaqStub'),
  HVCardStub = require('./HVCardStub'),
  SystemSummaryStub = require('./SystemSummaryStub'),
  FilterStationStub = require('./FilterStationStub'),
  EACSStub = require('./EACS/EACSStub'),
  BeamLineStub = require('./BeamLineStub'),
  AlarmServerStub = require('./AlarmServerStub'),
  AddhStub = require('./AddhStub');

/**
 * @typedef {{
 *   crateId: number,
 *   hostname?: string,
 *   layout?: Array<number>
 * }} NTOFStubDaqConfig
 *
 * @typedef {{
 *   cardId: number,
 *   nChannels: number
 * }} NTOFStubHVCardConfig
 *
 * @typedef {{
 *   port?: number,
 *   experimentalArea?: string,
 *   daqs: Array<NTOFStubDaqConfig>,
 *   hv?: Array<NTOFStubHVCardConfig>
 * }} NTOFStubConfig
 */

class NTOFStub {
  /*::
  started: boolean
  dns: typeof DnsServer
  eacs: EACSStub
  rfmerger: RFMergerStub
  alarmserver: AlarmServerStub
  addh: AddhStub
  daqs: Array<DaqStub>
  hv: Array<HVCardStub>
  systemsummary: SystemSummaryStub
  filterStation: FilterStationStub
  beamline: BeamLineStub
  timing: TimingStub
  config: NTOFStub$Config
  */
  /**
   * @param {NTOFStubConfig} config
   */
  constructor(config) {
    this.config = config;
  }

  close() {
    invoke(this.timing, 'close');
    invoke(this.eacs, 'close');
    invoke(this.rfmerger, 'close');
    invoke(this.node, 'close');
    invoke(this.dns, 'close');
    invoke(this.alarmserver, 'close');
    invoke(this.systemsummary, 'close');
    forEach(this.hv, (h) => invoke(h, 'close'));
    forEach(this.daqs, (d) => invoke(d, 'close'));
  }

  // eslint-disable-next-line max-statements
  async init() {
    if (this.dns) {
      debug('Init called twice !');
      return;
    }
    this.dns = new DnsServer(null, get(this.config, 'port', 2505));
    await this.dns.listen();
    this.node = new DisXmlNode(null, 0);

    this.timing = new TimingStub();
    this.rfmerger = new RFMergerStub();
    this.alarmserver = new AlarmServerStub();
    this.addh = new AddhStub();

    /** @type Array<DaqStub> */
    this.daqs = [];
    /** @type Array<string> */
    const hostnames = [];
    forEach(get(this.config, 'daqs'), (config, daqCount) => {
      const hostname = (config.hostname) ?
        config.hostname : (`ntofdaq-m${config.crateId}`);
      // @ts-ignore it is not undefined
      const daq = new DaqStub(this.dns.url());

      if (config.layout) {
        daq.clearCards();
        config.layout.forEach(
          (count, i) => daq.addCard(count, "S412", "SPD-" + daqCount + "00" + i));
      }
      daq.run();
      // @ts-ignore it is not undefined
      daq.register(hostname, this.node);
      hostnames.push(hostname);
      // @ts-ignore it is not undefined
      this.daqs.push(daq);
    });

    /** @type Array<HVCardStub> */
    this.hv = [];
    forEach(get(this.config, 'hv'), (config) => {
      const card = new HVCardStub(config.cardId, config.nChannels);
      // @ts-ignore it is not undefined
      card.register(this.node);
      // @ts-ignore it is not undefined
      this.hv.push(card);
    });

    this.systemsummary = new SystemSummaryStub();
    this.filterStation = new FilterStationStub();
    this.beamline = new BeamLineStub(
      get(this.config, [ 'beamline', 'vgr' ], 2),
      get(this.config, [ 'beamline', 'vpp' ], 2),
      get(this.config, [ 'beamline', 'vvs' ], 2));

    this.eacs = new EACSStub(hostnames, this.dns.url());
    this.eacs.eacsInfo.addFilterStation('FilterStation');
    forEach(get(this.config, 'hv'), (config) => {
      // @ts-ignore it is not undefined
      this.eacs.eacsInfo.addHighVoltage(config.cardId, config.nChannels);
    });

    this.timing.register(this.node);
    this.systemsummary.register(this.node);
    this.filterStation.register(this.node);
    this.beamline.register(this.node);
    this.rfmerger.register('MERGER', this.node);
    this.alarmserver.register(this.node);
    this.addh.register(this.node);
    this.eacs.register('EACS', this.node);
    await this.node.register(this.dns.url());

    // @ts-ignore it is not undefined
    this.timing.on('event', () => this.daqs.forEach((d) => d.trigger()));
    this.rfmerger.run();
    console.log("DNS listening on: " + this.dns.url().substr(6));
  }
}

module.exports = NTOFStub;
