const { toNumber } = require('lodash');

// @ts-check
const
  { DisXmlDataSet, XmlData } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:systemsummary'),
  os = require("os");

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

 const SysParams = {
  DISK: 0,
  RAM: 1,
  CPU: 2
};

class SystemSummaryStub extends DisXmlDataSet {
  constructor() {
    super();

    this.add({ name: "Disk used%", index: SysParams.DISK,
      unit: "%", type: XmlData.Type.DOUBLE, value: 10.1111 });
    this.add({ name: "RAM used%", index: SysParams.RAM,
      unit: "%", type: XmlData.Type.DOUBLE, value: this.getRamUsage() });
    this.add({ name: "Total CPU Usage", index: SysParams.CPU,
      unit: "%",  type: XmlData.Type.DOUBLE, value: 20.2222 });

    this._timer = setInterval(this.updateValues.bind(this), 1000);
  }

  close() {
    clearInterval(this._timer);
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlDataSet('localhost/SystemSummary', this);
    debug('system summary stub registered');
  }

  updateValues() {
    this.update(SysParams.DISK, this.getDiskUsage());
    this.update(SysParams.RAM, this.getRamUsage());
    this.update(SysParams.CPU, this.getCpuUsage());
  }

  getRamUsage() {
    const totalRAM = os.totalmem();
    const usedRAM = totalRAM - os.freemem();
    return (usedRAM * 100) / totalRAM;
  }

  getCpuUsage() {
    const currentValue = toNumber(this.get(SysParams.CPU) || 0);
    return (currentValue + 1.1111) % 50;
  }

  getDiskUsage() {
    const currentValue = toNumber(this.get(SysParams.DISK) || 0);
    return (currentValue + 1.1111) % 20;
  }

}

module.exports = SystemSummaryStub;
