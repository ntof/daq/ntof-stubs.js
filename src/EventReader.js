// @ts-check
const
  { DicXmlValue, xml } = require('@ntof/dim-xml'),
  _ = require('lodash'),
  debug = require('debug')('stub:EventReader'),
  { timeout, makeDeferred } = require('@cern/prom');

/**
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {{
 *   name: string,
 *   timestamp: number,
 *   cyclestamp: number,
 *   periodNB: number,
 *   dest: string,
 *   dest2: string,
 *   user: string,
 *   eventNumber: number
 * }} Event
 */

class EventReader extends DicXmlValue {
  /*::
  events: Array<Event>
  margin: number
  maxEvents: number
  */
  /**
   * @param {DnsClient | string} dns
   */
  constructor(dns) {
    super('Timing/event', { stamped: true }, dns);
    /** @type Array<Event> */
    this.events = [];
    this.margin = 100000000;
    this.maxEvents = 50;
  }

  /**
   * @param {any} rep
   */
  _setValue(rep) {
    // @ts-ignore _setValue exists (private)
    super._setValue(rep);
    if (this.value) {
      this._addEvent(_.get(xml.toJs(this.value), 'event.$'));
    }
  }

  /**
   * @param {Event} event
   */
  _addEvent(event) {
    _.forEach([ 'timestamp', 'cyclestamp', 'eventNumber', 'periodNB' ],
      function(/** @type keyof Event */prop) {
        // @ts-ignore it is a number
        event[prop] = _.toNumber(event[prop]);
      });
    this.events.splice(_.sortedIndexBy(this.events, event, 'timestamp'), 0,
      event);

    if (this.events.length >= this.maxEvents) {
      const count = this.events.length - this.maxEvents;

      debug('dropping events', count);
      this.events.splice(0, count);
    }
    this.emit('event', event);
  }

  /**
   * @param {number} time
   * @param {Event} event
   * @returns {boolean}
   */
  checkRange(time, event) {
    return Math.abs(time - event.timestamp) <= this.margin;
  }

  /**
   * @param {number} time
   * @param {number} tm timeout
   * @returns {Promise<Event>}
   */
  async getEventByTime(time, tm) {
    const index = _.findIndex(this.events, this.checkRange.bind(this, time));
    if (index >= 0) {
      this.events.splice(0, index);
      return Promise.resolve(this.events[index]);
    }

    /* waiting for event */
    const def = makeDeferred();
    const listener = (/** @type Event */event) => {
      if (this.checkRange(time, event)) {
        this.events.splice(0, _.indexOf(this.events, event));
        def.resolve(event);
      }
    };
    this.on('event', listener);
    return timeout(def.promise, tm || 1000, 'failed to find event')
    .finally(() => {
      this.removeListener('event', listener);
    });
  }
}

module.exports = EventReader;
