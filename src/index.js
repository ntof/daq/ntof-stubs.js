// @ts-check
const
  DaqStub = require('./daq/DaqStub'),
  RFMergerStub = require('./RFMerger/RFMergerStub'),
  TimingStub = require('./TimingStub'),
  EACSStub = require('./EACS/EACSStub'),
  NTOFStub = require('./NTOFStub'),
  BeamLineStub = require('./BeamLineStub'),
  AlarmServerStub = require('./AlarmServerStub'),
  AddhStub = require('./AddhStub');

module.exports = { DaqStub, RFMergerStub, TimingStub, EACSStub,
  NTOFStub, BeamLineStub, AlarmServerStub, AddhStub };
