// @ts-check
const
  { XmlData, DisXmlDataSet } = require('@ntof/dim-xml'),
  _ = require('lodash'),
  debug = require('debug')('stub:BeamLine');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

class GenericDipState extends DisXmlDataSet {
  /**
   * @param {string} svcName
   * @param {number} value
   */
  constructor(svcName, value = 0) {
    super();
    this.svcName = svcName;

    this.add({ name: "__DIP_DEFAULT__", index: 0, unit: "",
      type: XmlData.Type.INT64, value });
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlDataSet(this.svcName, this);
    debug('registered: %s', this.svcName);
  }
}

class GenericDipValue extends DisXmlDataSet {
  /**
   * @param {string} svcName
   * @param {number} value
   */
  constructor(svcName, value = 0) {
    super();
    this.svcName = svcName;

    this.add({ name: "__DIP_DEFAULT__", index: 0, unit: "",
      type: XmlData.Type.FLOAT, value });
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlDataSet(this.svcName, this);
    debug('registered: %s', this.svcName);
  }
}

class BeamLineStub {
  /**
  * @param {number} nVGR
  * @param {number} nVPP
  * @param {number} nVVS
  */
  constructor(nVGR, nVPP, nVVS) {
    debug('Creating Pump and Valve States and Values');
    this.vgrStates = _.times(nVGR, (index) => new GenericDipState(`TOF_VGR${index}.State`));
    this.vgrPr = _.times(nVGR, (index) => new GenericDipValue(`TOF_VGR${index}.PR`));
    this.vppStates = _.times(nVPP, (index) => new GenericDipState(`TOF_VPP${index}.State`));
    this.vvsStates = _.times(nVVS, (index) => new GenericDipState(`TOF.VVS${index}.State`));
    this.vvs = _.times(nVVS, (index) => new GenericDipValue(`Valves/TOF.VVS${index}`));
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    _.forEach(this.vgrStates, (item) => {
      item.register(node);
    });
    _.forEach(this.vgrPr, (item) => {
      item.register(node);
    });
    debug('All VGR registered [%d]', this.vgrStates.length);

    _.forEach(this.vppStates, (item) => {
      item.register(node);
    });
    debug('All VPP registered [%d]', this.vppStates.length);

    _.forEach(this.vvsStates, (item) => {
      item.register(node);
    });
    _.forEach(this.vvs, (item) => {
      item.register(node);
    });
    debug('All VVS registered [%d]', this.vvsStates.length);
  }
}

module.exports = BeamLineStub;
