// @ts-check
const
  _ = require('lodash'),
  EventEmitter = require('events'),
  debug = require('debug')('stub:rfm:engine'),

  RFMRun = require('./RFMergerRun');

class RawFileMergerEngine extends EventEmitter {
  constructor() {
    super();
    this.tickEngineCounter = 0;

    /** @type Map<number, RFMRun> */
    this.runs = new Map();
    this.globalStats = {
      incomplete: 0,
      waitingApproval: 0,
      waiting: 0,
      transferring: 0,
      copied: 0,
      migrated: 0,
      ignored: 0,
      failed: 0
    };
    this.globalTransferred = 0;
    this.globalRate = 0;

    debug('Generating random runs...');
    this.generateRuns();
    debug('Updating data...');
    this.updateData();

    this.tickEngine();
    debug('Configuring clocking ticker...');
    this._tickEngineTimer = setInterval(this.tickEngine.bind(this), 1000);
  }

  close() {
    debug('Closing...');
    clearInterval(this._tickEngineTimer);
  }

  start() {
    // Let's start current run ?
    // let cRun = this.getCurrentRun();
    // this.runStart(cRun.runNumber, 'exp_' + cRun.runNumber + '_test', true);
  }

  generateRuns() {
    const numRuns = 3;
    let runNumber = 900001;
    for (let i = 0; i < numRuns; i++) {
      this.runs.set(runNumber, new RFMRun(runNumber));
      this.runStop(runNumber);
      runNumber++;
    }
  }

  // By default is the highest runNumber
  /**
   * @returns {RFMRun?=}
   */
  getCurrentRun() {
    let maxRunNumber = -1;
    for (const item of this.runs) {
      maxRunNumber = Math.max(item[0], maxRunNumber);
    }
    if (maxRunNumber !== -1) { return this.runs.get(maxRunNumber); }
    return undefined;
  }

  updateData() {
    const newStat = {};
    this.globalTransferred = 0;
    this.globalRate = 0;
    this.runs.forEach((run) => {
      run.calculateStats();
      _.forEach(_.keys(run.stats), (key) => {
        _.set(newStat, key, _.get(newStat, key, 0) + _.get(run.stats, key, 0));
      });
      this.globalTransferred += run.transferred;
      this.globalRate += run.rate;
    });
    // @ts-ignore it's ok
    this.globalStats = newStat;
    this.emit('info-updated');
  }

  /**
   * @param {number} runNumber
   * @param {string?} experiment
   * @param {number?} approved
   * @param {number?} expiry
   * @returns {boolean}
   */ // eslint-disable-next-line complexity
  updateRun(runNumber, experiment = null, approved = null, expiry = null) {
    debug(`command updateRun
             runNumber [${runNumber}]
             experiment [${_.toString(experiment)}]
             approved [${_.toString(approved)}]
             expiry [${_.toString(expiry)}]`);
    const run = this.runs.get(runNumber);
    if (!run) { return false; }
    if (run.isMigrating) { return false; }

    // experiment - we can set it only if is not approved
    if (!_.isNil(experiment) && !run.approved) {
      run.experiment = experiment;
      debug(`Experiment set to [${experiment}] for run [${runNumber}]`);
    }
    // EXPIRY - we can set it only if is not approved
    if (!_.isNil(expiry) && !run.approved) {
      run.expiryDate = new Date(expiry * 1000);
      debug(`Expiry set to [${expiry}] for run [${runNumber}]`);
    }
    // approve - we can set it to 1 only and only if experiment is set
    if (approved === 1 && !run.approved && !_.isEmpty(run.experiment)) {
      run.approved = true;
      run.expiryDate = null;
      debug(`Approved set to [true] for run [${runNumber}]`);

      // Files status from waitingApproval -> waiting
      run.changeFilesStatus(RFMRun.FileStatus.waitingApproval, RFMRun.FileStatus.waiting);
      run.isMigrating = true;
    }
    this.updateData();
    return true;
  }

  /**
   * @param {number} runNumber
   * @param {string?} experiment
   * @param {number?=} approved
   * @returns {boolean}
   */
  runStart(runNumber, experiment, approved) {
    debug(`command runStart
             runNumber [${runNumber}]
             experiment [${_.toString(experiment)}]
             approved [${_.toString(approved)}]`);
    if (this.runs.get(runNumber)) { return false; }
    this.runs.set(runNumber, new RFMRun(runNumber));
    this.updateRun(runNumber, experiment, approved);
    return true;
  }

  /**
   * @param {number} runNumber
   * @returns {boolean}
   */
  runStop(runNumber) {
    debug(`command runStop runNumber [${runNumber}] `);
    const run = this.runs.get(runNumber);
    if (!run) { return false; }
    run.stopDate = new Date();
    if (!run.approved) {
      run.expiryDate = new Date();
      run.expiryDate.setDate(run.stopDate.getDate() + 30);
    }
    this.updateData();
    return true;
  }

  /**
   * @param {number} runNumber
   * @returns {boolean}
   */
  runDelete(runNumber) {
    debug(`command runDelete runNumber [${runNumber}] `);
    const ret = this.runs.delete(runNumber);
    if (ret) {
      debug(`runNumber [${runNumber}] deleted.`);
      this.updateData();
    }
    return ret;
  }

  /**
   * @param {number} runNumber
   * @param {number} fileNumber
   * @param {number} ignore
   * @returns {boolean}
   */
  fileIgnore(runNumber, fileNumber, ignore) {
    debug(`command fileIgnore
             runNumber [${runNumber}]
             fileNumber [${fileNumber}]
             ignore [${ignore}]`);
    const run = this.runs.get(runNumber);
    if (!run) { return false; }
    // fileNumber is equal to the index in the array
    if (ignore === undefined || ignore === 1) {
      const fileStatus = run.files[fileNumber].status;
      if (fileStatus !== "copied" && fileStatus !== "migrated") {
        run.files[fileNumber].ignored = true;
        run.files[fileNumber].status = RFMRun.FileStatus.ignored;
        run.files[fileNumber].rate = 0;
        debug(`runNumber [${runNumber}] fileNumber [${fileNumber}] ignored`);
      }
    }
    else {
      run.files[fileNumber].ignored = false;
      if (!run.approved) {
        run.files[fileNumber].status = RFMRun.FileStatus.waitingApproval;
      }
      else {
        run.files[fileNumber].status = RFMRun.FileStatus.waiting;
      }
      debug(`runNumber [${runNumber}] fileNumber [${fileNumber}] enabled back`);
    }
    this.updateData();
    return true;
  }

  tickEngine() {
    this.tickEngineCounter++;
    let dataChanged = false;
    this.runs.forEach((run) => {
      if (!run.isMigrating) { return; }
      dataChanged = true;
      run.changeFilesStatus(RFMRun.FileStatus.waiting, RFMRun.FileStatus.transferring);
      run.tickRun(RawFileMergerEngine.MAX_RATE_PER_FILE, this.tickEngineCounter);
    });
    if (dataChanged) { this.updateData(); }
  }

}

RawFileMergerEngine.MAX_RATE_PER_FILE = 5000; // B/s

module.exports = RawFileMergerEngine;
