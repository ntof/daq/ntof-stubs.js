// @ts-check
const
  { DisXmlParams, XmlData } = require('@ntof/dim-xml'),
  _ = require('lodash');

const {
  parseParamsReqXml,
  extractRunUpdatableParams,
  getUnixTime,
  ParamIndexRuns
} = require('./RFMUtils');


/**
 * @typedef {import('./RFMergerEngine')} RawFileMergerEngine
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 * @typedef {import('@ntof/dim-xml').DisXmlCmd.ReqInfo} DicXmlReqInfo
 * @typedef {import('@ntof/dim-xml').DisXmlDataSet.XmlDataInPart} XmlDataInPart
 */

class RFMRunsService extends DisXmlParams {
  /**
   * @param {RawFileMergerEngine} engine
   */
  constructor(engine) {
    super();
    this._engine = engine;
    this.handleParams();
  }

  /**
   * @param {DicXmlReqInfo} req
   */
  _handle(req) {
    const parameters = parseParamsReqXml(this.data, req);
    _.forEach(parameters, (runParams) => {
      const uParams = extractRunUpdatableParams(runParams.value, ParamIndexRuns);
      if (!uParams.runNumber || uParams.runNumber !== runParams.index) { return; }
      this._engine.updateRun(uParams.runNumber, uParams.experiment, uParams.approved, uParams.expiry);
    });
  }

  /**
   * @param {string} mode
   */
  handleParams(mode = 'add') {
    // handle MERGER/Current Params
    _.invoke(this, mode, { index: 0, name: 'Number of parameters',
      type: XmlData.Type.INT64, value: this._engine.runs.size });

    this._engine.runs.forEach((run) => {
      /** @type {{ name: 'run', index: number, value: Array<XmlDataInPart> }} */
      const runParam = { name: 'run', index: run.runNumber, value: [] };
      runParam.value.push({ index: ParamIndexRuns.RUN_NUMBER, name: 'runNumber',
        type: XmlData.Type.UINT32, value: run.runNumber });
      runParam.value.push({ index: ParamIndexRuns.EXPERIMENT, name: 'experiment',
        type: XmlData.Type.STRING, value: run.experiment });
      runParam.value.push({ index: ParamIndexRuns.APPROVED, name: 'approved',
        type: XmlData.Type.BOOL, value: _.toNumber(run.approved) });
      runParam.value.push({ index: ParamIndexRuns.START_DATE, name: 'startDate',
        type: XmlData.Type.UINT32, value: getUnixTime(run.startDate) });
      runParam.value.push({ index: ParamIndexRuns.STOP_DATE, name: 'stopDate',
        type: XmlData.Type.UINT32, value: getUnixTime(run.stopDate) });
      runParam.value.push({ index: ParamIndexRuns.EXPIRY, name: 'expiryDate',
        type: XmlData.Type.UINT32, value: getUnixTime(run.expiryDate) });
      runParam.value.push({ index: ParamIndexRuns.TRANSFERRED, name: 'transferred', unit: 'B',
        type: XmlData.Type.UINT64, value: run.transferred });
      runParam.value.push({ index: ParamIndexRuns.RATE, name: 'rate', unit: 'B/s',
        type: XmlData.Type.UINT64, value: run.rate });
      // Stats
      runParam.value.push({ index: ParamIndexRuns.INCOMPLETE, name: 'incomplete',
        type: XmlData.Type.UINT32, value: run.stats.incomplete });
      runParam.value.push({ index: ParamIndexRuns.WAITING_APPROVAL, name: 'waitingApproval',
        type: XmlData.Type.UINT32, value: run.stats.waitingApproval });
      runParam.value.push({ index: ParamIndexRuns.WAITING, name: 'waiting',
        type: XmlData.Type.UINT32, value: run.stats.waiting });
      runParam.value.push({ index: ParamIndexRuns.TRANSFERRING, name: 'transferring',
        type: XmlData.Type.UINT32, value: run.stats.transferring });
      runParam.value.push({ index: ParamIndexRuns.COPIED, name: 'copied',
        type: XmlData.Type.UINT32, value: run.stats.copied });
      runParam.value.push({ index: ParamIndexRuns.MIGRATED, name: 'migrated',
        type: XmlData.Type.UINT32, value: run.stats.migrated });
      runParam.value.push({ index: ParamIndexRuns.IGNORED, name: 'ignored',
        type: XmlData.Type.UINT32, value: run.stats.ignored });
      runParam.value.push({ index: ParamIndexRuns.FAILED, name: 'failed',
        type: XmlData.Type.UINT32, value: run.stats.failed });

      const runExt = this.get(run.runNumber);
      if (runExt) { // It means the run is already there
        _.invoke(this, 'update', runParam);
      }
      else {
        _.invoke(this, 'add', runParam);
      }
      // TODO remove runs when deleted...
    });
  }
}

module.exports = RFMRunsService;
