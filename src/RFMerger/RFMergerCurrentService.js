// @ts-check
const
  { DisXmlParams, XmlData } = require('@ntof/dim-xml'),
  _ = require('lodash');

const {
  parseParamsReqXml,
  extractRunUpdatableParams,
  getUnixTime,
  ParamIndexCurrent
} = require('./RFMUtils');

/**
 * @typedef {import('./RFMergerEngine')} RawFileMergerEngine
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 * @typedef {import('@ntof/dim-xml').DisXmlCmd.ReqInfo} DicXmlReqInfo
 */

class RFMCurrentService extends DisXmlParams {
  /**
   * @param {RawFileMergerEngine} engine
   */
  constructor(engine) {
    super();
    this._engine = engine;
    this.handleParams();
  }

  /**
   * @param {DicXmlReqInfo} req
   */
  _handle(req) {
    const runParams = parseParamsReqXml(this.data, req);
    const uParams = extractRunUpdatableParams(runParams, ParamIndexCurrent);
    if (!uParams.runNumber) { return; }
    const currentRun = this._engine.getCurrentRun();
    if (!currentRun) { return; }
    if (currentRun.runNumber !== uParams.runNumber) { return; } // maybe throw?
    this._engine.updateRun(uParams.runNumber, uParams.experiment, uParams.approved, uParams.expiry);
  }

  /**
   * @param {string} mode
   */
  handleParams(mode = 'add') {
    // handle MERGER/Current Params
    const currentRun = this._engine.getCurrentRun();
    if (!currentRun) { return; }
    // Check if we have something on the service dataset. If none, change mode to add.
    if (!this.get(0)) {
      mode = 'add';
    }

    _.invoke(this, mode, { index: ParamIndexCurrent.NUM_OF_PARAMS, name: 'Number of parameters',
      type: XmlData.Type.INT64, value: 17 });
    _.invoke(this, mode, { index: ParamIndexCurrent.RUN_NUMBER, name: 'runNumber',
      type: XmlData.Type.UINT32, value: currentRun.runNumber });
    _.invoke(this, mode, { index: ParamIndexCurrent.EXPERIMENT, name: 'experiment',
      type: XmlData.Type.STRING, value: currentRun.experiment });
    _.invoke(this, mode, { index: ParamIndexCurrent.APPROVED, name: 'approved',
      type: XmlData.Type.BOOL, value: _.toNumber(currentRun.approved) });
    _.invoke(this, mode, { index: ParamIndexCurrent.START_DATE, name: 'startDate',
      type: XmlData.Type.UINT32, value: getUnixTime(currentRun.startDate) });
    _.invoke(this, mode, { index: ParamIndexCurrent.STOP_DATE, name: 'stopDate',
      type: XmlData.Type.UINT32, value: getUnixTime(currentRun.stopDate) });
    _.invoke(this, mode, { index: ParamIndexCurrent.EXPIRY, name: 'expiryDate',
      type: XmlData.Type.UINT32, value: getUnixTime(currentRun.expiryDate) });
    _.invoke(this, mode, { index: ParamIndexCurrent.TRANSFERRED, name: 'transferred', unit: 'B',
      type: XmlData.Type.UINT64, value: currentRun.transferred });
    _.invoke(this, mode, { index: ParamIndexCurrent.RATE, name: 'rate', unit: 'B/s',
      type: XmlData.Type.UINT64, value: currentRun.rate });

    // Stats
    _.invoke(this, mode, { index: ParamIndexCurrent.INCOMPLETE, name: 'incomplete',
      type: XmlData.Type.UINT32, value: currentRun.stats.incomplete });
    _.invoke(this, mode, { index: ParamIndexCurrent.WAITING_APPROVAL, name: 'waitingApproval',
      type: XmlData.Type.UINT32, value: currentRun.stats.waitingApproval });
    _.invoke(this, mode, { index: ParamIndexCurrent.WAITING, name: 'waiting',
      type: XmlData.Type.UINT32, value: currentRun.stats.waiting });
    _.invoke(this, mode, { index: ParamIndexCurrent.TRANSFERRING, name: 'transferring',
      type: XmlData.Type.UINT32, value: currentRun.stats.transferring });
    _.invoke(this, mode, { index: ParamIndexCurrent.COPIED, name: 'copied',
      type: XmlData.Type.UINT32, value: currentRun.stats.copied });
    _.invoke(this, mode, { index: ParamIndexCurrent.MIGRATED, name: 'migrated',
      type: XmlData.Type.UINT32, value: currentRun.stats.migrated });
    _.invoke(this, mode, { index: ParamIndexCurrent.IGNORED, name: 'ignored',
      type: XmlData.Type.UINT32, value: currentRun.stats.ignored });
    _.invoke(this, mode, { index: ParamIndexCurrent.FAILED, name: 'failed',
      type: XmlData.Type.UINT32, value: currentRun.stats.failed });
  }
}

module.exports = RFMCurrentService;
