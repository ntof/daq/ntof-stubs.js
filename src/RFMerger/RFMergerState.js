// @ts-check
const
  {  DisXmlState } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:rfm:state');

const State = {
  OK: 0
};

class RawFileMergerState extends DisXmlState {
  constructor() {
    super();
    this.stateChanges = 0;
    this.addState(State.OK, "OK");
    this.setState(State.OK);
  }

  /**
   * @param {number} value
   * @returns {boolean}
   */
  setState(value) {
    ++this.stateChanges;
    debug('state change:', value);
    return super.setState(value);
  }
}

RawFileMergerState.State = State;

module.exports = RawFileMergerState;
