// @ts-check
const
  _ = require('lodash'),
  debug = require('debug')('stub:rfm:engine');

/**
 * @typedef {{
 *   incomplete: number,
 *   waitingApproval: number,
 *   waiting: number,
 *   transferring: number,
 *   copied: number,
 *   migrated: number,
 *   failed: number,
 *   ignored: number
 * }} Stats
 */

const FileStatus = {
  incomplete: "incomplete",
  waitingApproval: "waitingApproval",
  waiting: "waiting",
  transferring: "transferring",
  copied: "copied",
  migrated: "migrated",
  failed: "failed",
  ignored: "ignored"
};

class RFMRunFile {
  /**
   * @param {number} runNumber
   * @param {number} index
   */
  constructor(runNumber, index) {
    this.runNumber = runNumber;
    this.name = "run" + runNumber + "_" + index + "_s1.raw.finished";
    this.status = FileStatus.waitingApproval;
    this.ignored = false;
    // this.size = 20000000; // Bytes
    this.size = 200000; // Bytes
    this.transferred = 0;
    this.rate = 0;

    // engine stuff
    this.engineTicker = 0;
  }

  /**
   * @param {number} rate
   * @param {number} tickNumber
   */
  tickFile(rate, tickNumber) {
    if (this.status === FileStatus.transferring) {
      this.rate = rate;
      this.transferred += rate;

      // Check if file ended
      if (this.transferred >= this.size) {
        debug(`changing for run [${this.runNumber}] file [${this.name}] from status [${FileStatus.transferring}] to [${FileStatus.copied}]`);
        this.transferred = this.size;
        this.status = FileStatus.copied;
        this.rate = 0;
        this.engineTicker = tickNumber;
      }
    }

    // If copied, check if we can migrate (after 10 sec)
    if (this.status === FileStatus.copied && (tickNumber - this.engineTicker) >= 10) {
      debug(`changing for run [${this.runNumber}] file [${this.name}] from status [${FileStatus.copied}] to [${FileStatus.migrated}]`);
      this.status = FileStatus.migrated;
    }
  }
}

class RFMRun {
  /**
   * @param {number} runNumber
   */
  constructor(runNumber) {
    this.isMigrating = false;

    this.runNumber = runNumber;
    // this.experiment = "test_" + runNumber + "_exp";
    /** @type string? */
    this.experiment = null;
    this.approved = false;
    this.startDate = new Date();
    /** @type Date? */
    this.stopDate = null;
    /** @type Date? */
    this.expiryDate = null;
    this.transferred = 0;
    this.rate = 0;
    this.stats = {
      incomplete: 0,
      waitingApproval: 0,
      waiting: 0,
      transferring: 0,
      copied: 0,
      migrated: 0,
      ignored: 0,
      failed: 0
    };
    /** @type Array<RFMRunFile> */
    this.files = [];

    this.generateFiles();
    this.calculateStats();
  }

  generateFiles() {
    const numFiles = 150;
    for (let i = 0; i < numFiles; i++) {
      this.files.push(new RFMRunFile(this.runNumber, i));
    }
  }

  calculateStats() {
    // Iterate over all files.
    // eslint-disable-next-line guard-for-in
    for (const status in FileStatus) {
      // @ts-ignore that's ok
      const counter = _.countBy(this.files, (file) => file.status === FileStatus[status]);
      // counter.true keeps the counter we need
      _.set(this.stats, status, counter.true | 0);
    }
    this.transferred = _.sumBy(this.files, function(file) { return file.transferred; });
    this.rate = _.sumBy(this.files, function(file) { return file.rate; });
  }

  /**
   * @param {string} from
   * @param {string} to
   */
  changeFilesStatus(from, to) {
    for (let i = 0; i < this.files.length; i++) {
      if (this.files[i].status === from) {
        debug(`changing for run [${this.runNumber}] file [${this.files[i].name}] from status [${from}] to [${to}]`);
        this.files[i].status = to;
      }
    }
  }

  /**
   * @param {number} rate
   * @param {number} tickNumber
   */
  tickRun(rate, tickNumber) {
    // eslint-disable-next-line guard-for-in
    _.forEach(this.files, (file) =>  {
      file.tickFile(rate, tickNumber);
    });
    // TODO if all files are migrated or ignored, mark the run to be deleted
  }
}

RFMRun.FileStatus = FileStatus;

module.exports = RFMRun;
