// @ts-check
const
  { DisXmlDataSet, XmlData, xml } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:rfm'),
  _ = require('lodash'),

  RFMCurrentService = require('./RFMergerCurrentService'),
  RFMRunsService = require('./RFMergerRunsService'),
  RFMEngine = require('./RFMergerEngine'),
  RFMState = require('./RFMergerState');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 * @typedef {import('@ntof/dim-xml').DisXmlCmd} DisXmlCmd
 * @typedef {import('@ntof/dim-xml').DisXmlDataSet.XmlDataInPart} XmlDataInPart
 */

class RawFileMerger {
  constructor() {
    this._engine = new RFMEngine();
    this.rfmState = new RFMState();
    /** @type DisXmlDataSet */
    this.info = new DisXmlDataSet();
    /** @type RFMCurrentService */
    this.current = new RFMCurrentService(this._engine);
    /** @type RFMRunsService */
    this.runs = new RFMRunsService(this._engine);
    /** @type DisXmlDataSet */
    this.currentFiles = new DisXmlDataSet();
    this.currentFilesRunNumber = -1;
  }

  close() {
    this._engine.removeAllListeners('info-updated');
    this._engine.close();
  }

  /**
   * @param {string} name
   * @param {DisXmlNode} node
   */
  register(name, node) {

    // MERGER/State - It reports the current RawFile Merger state
    node.addXmlState(name + '/State', this.rfmState);
    debug(`${name}/State registered.`);

    // MERGER/Cmd - event (from EACS), runStart, runStop, runDelete, fileIgnore commands.
    node.addXmlCmd(name, (req) => {
      const cmd = xml.toJs(req.xml);
      return this.command(_.get(cmd, 'command'));
    });
    debug(`${name}/Cmd registered.`);

    // Merger/Info - It provides global stats about the RawFileMerger
    this.info = /** @type DisXmlDataSet */(node.addXmlDataSet(name + '/Info', this.info));
    this.handleInfoService('add');
    debug(`${name}/Info registered.`);

    // MERGER/Runs - It provides overall information about runs
    this.runs = /** @type RFMRunsService */(node.addXmlParams(name + '/Runs', this.runs));
    debug(`${name}/Runs registered.`);

    // MERGER/Current - Same as MERGER/Runs but provides information about current run only
    // In this stub current run = run with higher run number.
    this.current = /** @type RFMCurrentService */(node.addXmlParams(name + '/Current', this.current));
    debug(`${name}/Current registered.`);

    // MERGER/Current/Files - It provides live information about the latest run (ongoing or stopped).
    // In this stub current run = run with higher run number.
    this.currentFiles = /** @type DisXmlDataSet */(node.addXmlDataSet(name + '/Current/Files', this.currentFiles));
    this.handleCurrentFilesService('add');
    debug(`${name}/Current/Files registered.`);

    // MERGER/Files - It provides information about files associated with a given run.
    node.addXmlRpc(name + '/Files', this.filesRpcCallback.bind(this));
    debug(`${name}/Files registered.`);

    // When engine ticks and modify data, we need to update all our services
    this._engine.on('info-updated', this.updateServices.bind(this));
  }

  run() {
    this._engine.start();
  }

  updateServices() {
    this.handleInfoService('update');
    this.handleCurrentFilesService('update');
    this.current.handleParams('update');
    this.runs.handleParams('update');
  }

  /**
   * @param {any} req
   * @param {Element} ret
   */
  filesRpcCallback(req, ret) {
    var params = _.get(xml.toJs(req.xml), 'command');
    if (_.get(params, '$.name') === 'listFiles') {
      const runNumber = _.toNumber(_.get(params, '$.runNumber'));
      if (!runNumber) { return; }
      const run = this._engine.runs.get(runNumber);
      if (!run) { return; }
      params.data = [];
      params.data.push({ '$': { name: "runNumber", index: 0, type: XmlData.Type.UINT32, value: runNumber } });
      let index = 0;
      const files = _.map(run.files, (file) => ({
        '$': { index: index++, name: file.name },
        data: [
          { '$': { index: 0, name: 'status', type: XmlData.Type.STRING, value: file.status } },
          { '$': { index: 1, name: 'ignored', type: XmlData.Type.BOOL, value: _.toNumber(file.ignored) } },
          { '$': { index: 2, name: 'size', unit: 'B', type: XmlData.Type.UINT64, value: file.size } },
          { '$': { index: 3, name: 'transferred', unit: 'B', type: XmlData.Type.UINT64, value: file.transferred } }
        ]
      }));
      params.data.push({ '$': { name: "files", index: 1 }, data: files });
      // params.data = _.concat(params.data, files);
      xml.fromJs(ret, params);
    }
  }

  /**
   * @param {string} mode
   */
  handleInfoService(mode = 'add') {
    // handle MERGER/Info DataSet
    let index = 0; // It will match always the same index in both mode
    _.forEach(this._engine.globalStats, (value, name) => {
      _.invoke(this.info, mode, { index: index++, name, type: XmlData.Type.UINT32, value });
    });
    _.invoke(this.info, mode, { index: index++, name: 'transferred', unit: 'B',
      type: XmlData.Type.UINT64, value: this._engine.globalTransferred });
    _.invoke(this.info, mode, { index: index++, name: 'rate', unit: 'B/s',
      type: XmlData.Type.UINT64, value: this._engine.globalRate });
  }

  /**
   * @param {string} mode
   */
  handleCurrentFilesService(mode = 'add') {
    // handle MERGER/Current/Files DataSet
    let index = 0; // It will match always the same index in both mode
    const currentRun = this._engine.getCurrentRun();
    if (!currentRun) { return; }
    // Check if we have something on the service dataset. If none, change mode to add.
    if (!this.currentFiles.get(0)) {
      mode = 'add';
    }

    if (mode === 'add') {
      this.currentFilesRunNumber = currentRun.runNumber;
    }
    _.invoke(this.currentFiles, mode, { index: index++, name: 'runNumber',
      type: XmlData.Type.UINT32, value: currentRun.runNumber });

    /** @type Array<XmlDataInPart> */
    const files = [];
    let indexFiles = 0;
    _.forEach(currentRun.files, (file) => {
      const fileData = [];
      let indexData = 0;
      fileData.push({ index: indexData++, name: 'status',
        type: XmlData.Type.STRING, value: file.status });
      fileData.push({ index: indexData++, name: 'ignored',
        type: XmlData.Type.BOOL, value: _.toNumber(file.ignored) });
      fileData.push({ index: indexData++, name: 'size', unit: 'B',
        type: XmlData.Type.UINT64, value: file.size });
      fileData.push({ index: indexData++, name: 'transferred', unit: 'B',
        type: XmlData.Type.UINT64, value: file.transferred });
      fileData.push({ index: indexData++, name: 'rate', unit: 'B/s',
        type: XmlData.Type.UINT64, value: file.rate });
      files.push({ index: indexFiles++, name: file.name, value: fileData });
    });

    if (this.currentFilesRunNumber !== currentRun.runNumber) {
      this.currentFiles.remove(1); // we remove files
      _.invoke(this.currentFiles, 'add', { index: index++, name: 'files', value: files });
    }
    else {
      _.invoke(this.currentFiles, mode, { index: index++, name: 'files', value: files });
    }
  }

  /**
   * @param {string} cmd
   */ // eslint-disable-next-line complexity
  command(cmd) {
    const command = _.get(cmd, "$.name", null);
    if (!command || command === "event") { return; /* EACS command */}
    else {
      const runNumber = _.toNumber(_.get(cmd, "$.runNumber"));
      if (_.isNaN(runNumber)) { return; }
      if (command === "runStop") {
        debug('executing command runStop for', runNumber);
        _.defer(() => this._engine.runStop(runNumber));
      }
      if (command === "runDelete") {
        debug('executing command runDelete for', runNumber);
        _.defer(() => this._engine.runDelete(runNumber));
      }
      if (command === "runStart") {
        const experiment = _.get(cmd, "$.experiment");
        /** @type number | undefined */
        let approved = _.toNumber(_.get(cmd, "$.approved"));
        if (_.isNaN(approved)) { approved = undefined; }
        debug('executing command runStart for', runNumber, experiment, approved);
        _.defer(() => this._engine.runStart(runNumber, experiment, approved));
      }
      if (command === "fileIgnore") {
        const fileNumber = _.toNumber(_.get(cmd, "$.fileNumber"));
        if (_.isNaN(fileNumber)) { return; }
        let ignore = _.toNumber(_.get(cmd, "$.ignore"));
        if (_.isNaN(ignore)) { ignore = 1; }
        debug('executing command fileIgnore for', runNumber, fileNumber, ignore);
        _.defer(() => this._engine.fileIgnore(runNumber, fileNumber, ignore));
      }
    }
  }
}

module.exports = RawFileMerger;
