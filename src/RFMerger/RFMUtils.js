// @ts-check
const
  { xml } = require('@ntof/dim-xml'),
  _ = require('lodash');

/**
 * @typedef {import('./RFMergerEngine')} RawFileMergerEngine
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 * @typedef {import('@ntof/dim-xml').DisXmlCmd.ReqInfo} DicXmlReqInfo
 *
 * @typedef {Partial<{
 *   runNumber: number;
 *   experiment: string;
 *   approved: number;
 *   expiry: number;
 * }>} UpdatableParams
 */

const ParamIndexRuns = {
  RUN_NUMBER: 0,
  EXPERIMENT: 1,
  APPROVED: 2,
  START_DATE: 3,
  STOP_DATE: 4,
  EXPIRY: 5,
  TRANSFERRED: 6,
  RATE: 7,
  INCOMPLETE: 8,
  WAITING_APPROVAL: 9,
  WAITING: 10,
  TRANSFERRING: 11,
  COPIED: 12,
  MIGRATED: 13,
  IGNORED: 14,
  FAILED: 15
};

const ParamIndexCurrent = {
  NUM_OF_PARAMS: 0,
  RUN_NUMBER: 1,
  EXPERIMENT: 2,
  APPROVED: 3,
  START_DATE: 4,
  STOP_DATE: 5,
  EXPIRY: 6,
  TRANSFERRED: 7,
  RATE: 8,
  INCOMPLETE: 9,
  WAITING_APPROVAL: 10,
  WAITING: 11,
  TRANSFERRING: 12,
  COPIED: 13,
  MIGRATED: 14,
  IGNORED: 15,
  FAILED: 16
};

/**
 * @param {any} elt
 * @param {number|string|Array<any>|boolean} value
 * @param {any?} type
 * @returns {boolean}
 */
function isSameType(elt, value, type) {
  if (!_.isNil(type)) {
    return elt.type === type;
  }
  return typeof elt.value === typeof value;
}

/**
 * @param {Element} data
 * @returns {any}
 */
function parseData(data) {
  if (_.isEmpty(data.getAttribute('type')) &&
    _.isEmpty(data.getAttribute('value'))) {
    return {
      value: _.map(xml.getChildNodes(data, 'data'), parseData),
      index: _.toNumber(data.getAttribute('index'))
    };
  }
  else {
    return {
      index: _.toNumber(data.getAttribute('index')),
      type: _.toNumber(data.getAttribute('type')),
      value: data.getAttribute('value')
    };
  }
}

/**
 * @param {any} dataset
 * @param {any} data
 * @returns {boolean}
 */
function checkParam(dataset, data) {
  const index = _.get(data, 'index');
  if (!data || index < 0) { return false; }

  const param = _.find(dataset, { index });
  if (!param) { return false; }

  if (_.isArray(data.value)) {
    let ret = true;
    _.forEach(data.value, (nestedValue) => {
      ret = checkParam(param.value, nestedValue) && ret;
    });
    return ret;
  }
  else {
    return !_.isNil(data.value) &&
      isSameType(param, data.value, data.type);
  }
}

/**
 * @param {any} dataset
 * @param {DicXmlReqInfo} req
 * @returns {any}
 */
function parseParamsReqXml(dataset, req) {
  if (!req || !req.xml) { throw 'invalid request: xml not found'; }
  var elt = xml.getChildNodes(req.xml, 'parameters')[0];
  if (!elt) { throw 'invalid request: missing parameters'; }
  var parameters = _.map(xml.getChildNodes(elt, 'data'), parseData);
  var fail = _.find(parameters,
    (data) => !checkParam(dataset, data));
  if (fail) {
    throw 'invalid request: failed to validate attr:' +
    _.toString(fail.index);
  }
  return parameters;
}

/**
 * @param {any} runParams
 * @param {ParamIndexRuns | ParamIndexCurrent} ParamIndex
 * @returns {UpdatableParams}
 */
function extractRunUpdatableParams(runParams, ParamIndex) {
  const ret = {
    runNumber: undefined,
    experiment: undefined,
    approved: undefined,
    expiry: undefined
  };
  const runNumberParam = _.filter(runParams, (o) => o.index === ParamIndex.RUN_NUMBER)[0];
  const experimentParam = _.filter(runParams, (o) => o.index === ParamIndex.EXPERIMENT)[0];
  const approvedParam = _.filter(runParams, (o) => o.index === ParamIndex.APPROVED)[0];
  const expiryParam = _.filter(runParams, (o) => o.index === ParamIndex.EXPIRY)[0];
  // All the other parameters, if provided, are ignored.
  if (runNumberParam) { _.set(ret, 'runNumber', _.toNumber(runNumberParam.value)); }
  if (experimentParam) { _.set(ret, 'experiment', experimentParam.value); }
  if (approvedParam) { _.set(ret, 'approved', _.toNumber(approvedParam.value)); }
  if (expiryParam) { _.set(ret, 'expiry', _.toNumber(expiryParam.value)); }
  return ret;
}

/**
 * @param {Date?} date
 * @returns {number}
 */
function getUnixTime(date) {
  return date ? Math.trunc(date.getTime() / 1000) : 0;
}

module.exports = {
  parseParamsReqXml,
  extractRunUpdatableParams,
  getUnixTime,
  ParamIndexRuns,
  ParamIndexCurrent
};
