// @ts-check
const
  { DisXmlParams, XmlData } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:FilterStation');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

const FILTERS_COUNT = 8;
const ParamIdx = {
  AIR_PRESSURE: 1,
  FILTER_FIRST: 2,
  FILTER_LAST: 9
};

const Position = {
  UNKNOWN: 0,
  IN: 1,
  OUT: 2,
  MOVING: 3,
  IN_INTERLOCKED: 4,
  OUT_INTERLOCKED: 5,
  MOVING_INTERLOCKED: 6
};

class FilterStationStub extends DisXmlParams {
  constructor() {
    super();
    this.add({ name: "Air Pressure", index: ParamIdx.AIR_PRESSURE, unit: "Bar",
      type: XmlData.Type.FLOAT, value: 10 });

    for (let i = 0; i < FILTERS_COUNT; ++i) {
      this.add({
        type: XmlData.Type.ENUM, value: Position.IN, valueName: "IN",
        enum: [
          { value: Position.UNKNOWN, name: "UNKNOWN" },
          { value: Position.IN, name: "IN" },
          { value: Position.OUT, name: "OUT" },
          { value: Position.MOVING, name: "MOVING" },
          { value: Position.IN_INTERLOCKED, name: "IN_INTERLOCKED" },
          { value: Position.OUT_INTERLOCKED, name: "OUT_INTERLOCKED" },
          { value: Position.MOVING_INTERLOCKED, name: "MOVING_INTERLOCKED" }
        ]
      });
    }
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlParams('FilterStation', this);
    debug('stub registered');
  }
}
FilterStationStub.Params = ParamIdx;
FilterStationStub.Position = Position;

module.exports = FilterStationStub;
