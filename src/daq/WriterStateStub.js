// @ts-check
const { DisXmlState } = require('@ntof/dim-xml');

const State = {
  IDLE: 0,
  INITIALIZATION: 1,
  ALLOCATING_MEMORY: 2,
  WAITING_FOR_DATA: 3,
  PROCESSING_DATA: 4,
  WRITING_DATA: 5,
  FREEING_BUFFERS: 6,
  STOPPING: 7
};

class WriterState extends DisXmlState {
  constructor() {
    super();
    this.addState(State.IDLE, "Idle");
    this.addState(State.INITIALIZATION, "Initialization");
    this.addState(State.ALLOCATING_MEMORY, "Allocating memory");
    this.addState(State.WAITING_FOR_DATA, "Waiting for data");
    this.addState(State.PROCESSING_DATA, "Processing data");
    this.addState(State.WRITING_DATA, "Writing data to disk");
    this.addState(State.FREEING_BUFFERS, "Freeing buffers");
    this.addState(State.STOPPING, "Stopping thread");
    this.setState(State.IDLE);
  }
}

WriterState.State = State;

module.exports = WriterState;
