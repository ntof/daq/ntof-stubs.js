// @ts-check
const
  Channel = require("./ChannelStub"),
  EventEmitter = require('events'),
  _ = require("lodash");

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

class Cards extends EventEmitter {
  /**
   * @param {number} index
   * @param {number} nbChannel
   * @param {string} type
   * @param {string} serialNumber
   */
  constructor(index, nbChannel, type, serialNumber) {
    super();
    this.index = index;
    /** @type Array<Channel> */
    this.channels = [];
    this.type = type;
    this.serialNumber = serialNumber;

    for (var i = 0; i < nbChannel; ++i) {
      this.addChannel();
    }
  }

  /**
   * @returns {Channel}
   */
  addChannel() {
    const chan = new Channel(this.channels.length);
    chan.on('configured', this._checkConfig.bind(this));
    this.channels.push(chan);
    return chan;
  }

  /**
   * @param {string} name
   * @param {DisXmlNode} node
   */
  register(name, node) {
    _.forEach(this.channels, (channel) => {
      node.addXmlParams(name + '/CARD' + this.index + "/CHANNEL" + channel.index, channel);
      node.addXmlDataSet(name + '/CARD' + this.index + "/CHANNEL" + channel.index + '/Calibration', channel.calibService);
    });
  }

  _checkConfig() {
    if (this.isConfigured()) {
      this.emit('configured');
    }
  }

  /**
   * @returns {boolean}
   */
  isConfigured() {
    return _.every(this.channels, (channel) => channel.isConfigured());
  }

  /**
   * @param {boolean} value
   */
  setConfigured(value) {
    _.forEach(this.channels, (channel) => channel.setConfigured(value));
  }
}

module.exports = Cards;
