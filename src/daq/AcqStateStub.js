// @ts-check
const { DisXmlState } = require('@ntof/dim-xml');

const State = {
  IDLE: 0,
  INITIALIZATION: 1,
  ALLOCATING_MEMORY: 2,
  WAITING_TO_START: 3,
  ARMING_TRIGGERS: 4,
  WAITING_FOR_TRIGGER: 5,
  DUMPING_CARDS: 6,
  QUEUING_BUFFERS: 7
};

class AcqState extends DisXmlState {
  /*::
  static State: typeof State
  */
  constructor() {
    super();
    this.addState(State.IDLE, "Idle");
    this.addState(State.INITIALIZATION, "Initialization");
    this.addState(State.ALLOCATING_MEMORY, "Allocating memory");
    this.addState(State.WAITING_TO_START, "Waiting to start");
    this.addState(State.ARMING_TRIGGERS, "Arming card triggers");
    this.addState(State.WAITING_FOR_TRIGGER, "Waiting for trigger");
    this.addState(State.DUMPING_CARDS, "Dumping card memory buffers");
    this.addState(State.QUEUING_BUFFERS, "Queuing buffers for writer");
    this.setState(State.IDLE);
  }
}

AcqState.State = State;

module.exports = AcqState;
