// @ts-check
const
  { DisXmlState } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:Daq');

const State = {
  HARDWARE_INIT: 0,
  NOT_CONFIGURED: 1,
  CALIBRATING: 2,
  WAITING_FOR_COMMAND: 3,
  PROCESSING_COMMAND: 4,
  STOPPING_ACQ: 8,
  STARTING_ACQ: 9,
  RESETING: 10,
  INITIALIZING_ACQ: 11,
  WAITING_FOR_START_ACQ: 12,
  SUPER_USER: 13
};

class DaqState extends DisXmlState {
  constructor() {
    super();
    this.stateChanges = 0;
    this.addState(State.HARDWARE_INIT, "Hardware initialization");
    this.addState(State.NOT_CONFIGURED, "Not configured");
    this.addState(State.CALIBRATING, "Calibrating cards");
    this.addState(State.WAITING_FOR_COMMAND, "Waiting for command");
    this.addState(State.PROCESSING_COMMAND, "Processing command");
    this.addState(State.STOPPING_ACQ, "Stopping acquisition");
    this.addState(State.STARTING_ACQ, "Starting acquisition");
    this.addState(State.RESETING, "Reseting");
    this.addState(State.INITIALIZING_ACQ, "Initializing acquisition");
    this.addState(State.WAITING_FOR_START_ACQ,
      "Waiting for start acquisition command");
    this.addState(State.SUPER_USER, "Super user");
    this.setState(State.HARDWARE_INIT);
  }

  /**
   * @param {number} value
   * @returns {boolean}
   */
  setState(value) {
    ++this.stateChanges;
    debug('state change:', value);
    return super.setState(value);
  }
}

DaqState.State = State;

module.exports = DaqState;
