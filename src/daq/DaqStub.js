/* eslint-disable max-lines */
// @ts-check
const
  DaqState = require("./DaqStateStub"),
  WriterState = require("./WriterStateStub"),
  AcqState = require("./AcqStateStub"),
  Card = require("./CardStub"),
  ZeroSuppression = require("./ZeroSuppression"),
  EventReader = require('../EventReader'),

  { xml } = require('@ntof/dim-xml'),
  { DisService } = require('@cern/dim'),
  _ = require('lodash'),
  { expect } = require('chai'),
  debug = require('debug')('stub:Daq'),
  url = require('url'),
  { delay } = require('@cern/prom');

/**
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@ntof/dim-xml').DisXmlParams} DisXmlParams
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

class Daq {
  /**
   * @param {DnsClient| string} dns
   */
  constructor(dns) {
    this.daqState = new DaqState();
    this.writerState = new WriterState();
    this.acqState = new AcqState();
    this.runNumber = -1;
    /** @type Array<Card> */
    this.cards = [];
    this.listService = new DisService('C', this.buildListDaqElements());
    this.zspService = new ZeroSuppression(this);
    this.runExtNumService =
      new DisService('C', this.buildRunExtNum(this.runNumber, -1, 0));

    this.addCard(4, "S412", "SPD-02913");
    this.addCard(4, "S412", "SPD-02914");

    this.eventReader = new EventReader(dns);
  }

  /**
   * @param {string} name
   * @param {DisXmlNode} node
   */
  register(name, node) {
    node.addXmlState(name + '/DaqState', this.daqState);
    node.addXmlState(name + '/WriterState', this.writerState);
    node.addXmlState(name + '/AcquisitionState', this.acqState);

    node.addXmlCmd(name + '/Daq/Command', (req) => {
      const cmd = xml.toJs(req.xml);
      return this.command(_.get(cmd, 'command.command'));
    });

    _.forEach(this.cards, (card) => {
      card.register(name, node);
    });

    node.addService(name + '/ListDaqElements', this.listService);
    node.addXmlParams(name + '/ZeroSuppression', this.zspService);
    node.addService(name + '/WRITER/RunExtensionNumber', this.runExtNumService);
  }

  close() {
    this.eventReader.release();
  }

  /**
   * @returns {Promise<void>}
   */
  async run() {
    expect(this.daqState.states.value).to.equal(DaqState.State.HARDWARE_INIT);
    this.daqState.setState(DaqState.State.NOT_CONFIGURED);
    return this.configEvent(); /* check if daq is configured */
  }

  /**
   * @returns {boolean}
   */
  isRunning() {
    return this.acqState.states.value > AcqState.State.WAITING_TO_START;
  }

  async stop() {
    try {
      this.daqState.setState(DaqState.State.WAITING_FOR_COMMAND);
      await delay(100);
      this.daqState.setState(DaqState.State.STOPPING_ACQ);
      const changes = this.daqState.stateChanges;
      await delay(100);

      this._check(changes);
      this.acqState.setState(AcqState.State.IDLE);
      this.writerState.setState(WriterState.State.IDLE);
      _.forEach(this.cards, (c) => c.setConfigured(false));
      this.daqState.setState(DaqState.State.NOT_CONFIGURED);
    }
    catch (err) {
      debug(err);
    }
  }

  /**
   * @param {number} runNumber
   */
  async init(runNumber) {
    try {
      this.runNumber = runNumber;
      this.runExtNumService.setValue(this.buildRunExtNum(-1, -1, 0));

      this.daqState.setState(DaqState.State.PROCESSING_COMMAND);
      let changes = this.daqState.stateChanges;
      await delay(100);

      this._check(changes);
      this.daqState.setState(DaqState.State.INITIALIZING_ACQ);
      this.writerState.setState(WriterState.State.INITIALIZATION);
      this.acqState.setState(AcqState.State.INITIALIZATION);
      changes = this.daqState.stateChanges;
      await delay(100);

      this._check(changes);
      this.acqState.setState(AcqState.State.ALLOCATING_MEMORY);
      this.writerState.setState(WriterState.State.ALLOCATING_MEMORY);
      changes = this.daqState.stateChanges;
      await delay(100);
      this._check(changes);
      this.acqState.setState(AcqState.State.WAITING_TO_START);
      this.writerState.setState(WriterState.State.WAITING_FOR_DATA);
      this.daqState.setState(DaqState.State.WAITING_FOR_START_ACQ);
    }
    catch (err) {
      debug(err);
    }
  }

  async start() {
    try {
      this.daqState.setState(DaqState.State.STARTING_ACQ);
      this.acqState.setState(AcqState.State.ARMING_TRIGGERS);
      const changes = this.daqState.stateChanges;
      await delay(1000); /* 1 sec delay to emulate real daq startup) */

      this._check(changes);
      this.daqState.setState(DaqState.State.WAITING_FOR_COMMAND);
      this.acqState.setState(AcqState.State.WAITING_FOR_TRIGGER);
    }
    catch (err) {
      debug(err);
    }
  }

  async reset() {
    try {
      this.daqState.setState(DaqState.State.RESETING);
      const changes = this.daqState.stateChanges;
      await delay(100);

      this._check(changes);
      this.acqState.setState(AcqState.State.IDLE);
      this.writerState.setState(WriterState.State.IDLE);
      _.forEach(this.cards, (c) => c.setConfigured(false));
      this.daqState.setState(DaqState.State.NOT_CONFIGURED);
    }
    catch (err) {
      debug(err);
    }
  }

  /**
   * @param {string} cmd
   * @returns {Promise<void>}
   */
  command(cmd) {
    var cmdUrl = url.parse(cmd, true);
    if (cmdUrl.pathname === "reset") {
      return this.reset();
    }
    else if (cmdUrl.pathname === "initialization") {
      expect(this.daqState.states.value)
      .to.equal(DaqState.State.WAITING_FOR_COMMAND);

      var runNumber = _.toNumber(_.get(cmdUrl, 'query.runnumber',
        this.runNumber));
      return this.init(runNumber);
    }
    else if (cmdUrl.pathname === "stop") {
      expect(this.isRunning()).to.be.true();
      expect(this.daqState.states.value)
      .to.equal(DaqState.State.WAITING_FOR_COMMAND);
      return this.stop();
    }
    else if (cmdUrl.pathname === "start") {
      expect(this.daqState.states.value)
      .to.equal(DaqState.State.WAITING_FOR_START_ACQ);
      return this.start();
    }
    else if (cmdUrl.pathname === "calibrate") {
      expect(this.daqState.states.value)
      .to.equal(DaqState.State.WAITING_FOR_COMMAND);
      return Promise.resolve();
    }
    expect.fail('unknown command: ' + _.toString(cmdUrl.pathname));
    return Promise.resolve();
  }

  clearCards() {
    _.forEach(this.cards, (c) => c.removeAllListeners('configured'));
    this.cards = [];
    this.listService.setValue(this.buildListDaqElements());
  }

  /**
   * @param {number} nbChannel
   * @param {string} type
   * @param {string} serialNumber
   * @returns {Card}
   */
  addCard(nbChannel, type, serialNumber) {
    const card = new Card(this.cards.length, nbChannel, type, serialNumber);
    card.on('configured', this.configEvent.bind(this));
    this.cards.push(card);
    this.listService.setValue(this.buildListDaqElements());
    return card;
  }

  /**
   * @returns {boolean}
   */
  isConfigured() {
    return _.every(this.cards, (c) => c.isConfigured());
  }

  /**
   * @returns {string}
   */
  buildListDaqElements() {
    var ret = "<listDaqElements>\n";
    _.forEach(this.cards, (card) => {
      ret = ret + `<card${card.index} nbChannel="${card.channels.length}" type="${card.type}" serialNumber="${card.serialNumber}" />\n`;
    });
    return ret + "</listDaqElements>";
  }

  /**
   * @param {string} sn
   * @param {number} index
   */
  checkChannelExist(sn, index) {
    var cardVar = _.find(this.cards, { 'serialNumber': sn });

    expect(cardVar, 'unknown card ' + sn).to.be.not.undefined();
    // @ts-ignore it is not undefined
    expect(cardVar.channels, 'invalid index ' + index).to.have.length.above(index);
  }

  /**
   * @param {number} runNumber
   * @param {number} event
   * @param {number} size
   * @returns {string}
   */
  buildRunExtNum(runNumber, event, size) {
    return `<RunExtensionNumberWrote run="${runNumber}" event="${event}" size="${size}"/>`;
  }

  async trigger() { // eslint-disable-line max-statements
    if ((this.daqState.states.value !== DaqState.State.WAITING_FOR_COMMAND) ||
        (this.acqState.states.value !== AcqState.State.WAITING_FOR_TRIGGER)) {
      debug('ignoring trigger acqState:%i', this.acqState.states.value);
      return;
    }

    try {
      const changes = this.daqState.stateChanges;
      /* FIXME raise a warning on error here */

      const event = await this.eventReader.getEventByTime(Date.now() * 1000000, 1000);
      this._check(changes);
      this.acqState.setState(AcqState.State.DUMPING_CARDS);
      await delay(100);

      this._check(changes);
      this.acqState.setState(AcqState.State.QUEUING_BUFFERS);
      this.writerState.setState(WriterState.State.PROCESSING_DATA);

      await delay(100);
      this._check(changes);

      this.writerState.setState(WriterState.State.WRITING_DATA);
      await delay(100);
      this._check(changes);
      this.runExtNumService.setValue(
        this.buildRunExtNum(this.runNumber, event.eventNumber, 0));

      await delay(100);
      this._check(changes);
      this.writerState.setState(WriterState.State.FREEING_BUFFERS);
      await delay(100);
      this._check(changes);
      this.writerState.setState(WriterState.State.WAITING_FOR_DATA);
      await delay(100);
      this._check(changes);
      this.acqState.setState(AcqState.State.ARMING_TRIGGERS);
      await delay(100);
      this._check(changes);
      this.acqState.setState(AcqState.State.WAITING_FOR_TRIGGER);
    }
    catch (err) {
      debug(err);
    }
  }

  async configEvent() {
    if ((this.daqState.states.value === DaqState.State.NOT_CONFIGURED) &&
        this.isConfigured()) {
      debug('configured');

      try {
        let changes = this.daqState.stateChanges;
        await delay(100);
        this._check(changes);
        this.daqState.setState(DaqState.State.CALIBRATING);

        changes = this.daqState.stateChanges;
        await delay(100);
        this._check(changes);
        this.daqState.setState(DaqState.State.WAITING_FOR_COMMAND);
      }
      catch (err) {
        debug(err);
      }
    }
  }

  /**
   * @param {number} stateChanges
   */
  _check(stateChanges) {
    expect(stateChanges)
    .to.equal(this.daqState.stateChanges, 'Daq state changed, aborting');
  }
}
module.exports = Daq;
