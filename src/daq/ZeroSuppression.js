// @ts-check
const
  { assign, forEach, toNumber, isNil, map, find, get } = require('lodash'),
  { expect } = require('chai'),
  { DisXmlParams, XmlData, xml } = require('@ntof/dim-xml');

/**
 * @typedef {import('./DaqStub')} DaqStub
 * @typedef {import('@ntof/dim-xml').DicXmlParams.XmlParamPart} XmlParamPart
 */

const Mode = {
  INDEPENDENT: 0,
  SINGLE_MASTER: 1,
  MASTER: 2
};

const ParamMap = {
  MODE: 1,
  CONFIGURATION: 2
};

const ParamChannelMap = {
  SN: 0,
  CHANNEL: 1
};

class ZeroSuppression extends DisXmlParams {
  /**
   * @param {DaqStub?=} daq
   */
  constructor(daq) {
    super();
    this.daq = daq;
    this.add({ name: "Number of parameters", index: 0, unit: '', type: XmlData.Type.INT32, value: 2 });
    this.add({ name: "mode", index: ParamMap.MODE, unit: '', type: XmlData.Type.ENUM,
      value: Mode.INDEPENDENT, valueName: "independent",
      enum: [
        { value: Mode.INDEPENDENT, name: "independent" },
        { value: Mode.SINGLE_MASTER, name: "singlemaster" },
        { value: Mode.MASTER, name: "master" }
      ]
    });
    this.add({ name: "configuration", index: ParamMap.CONFIGURATION, type: XmlData.Type.NESTED, value: [] });
  }

  /**
   * @param {XmlParamPart} data
   * @param {boolean} isMaster
   * @returns {XmlParamPart}
   */
  parseChannel(data, isMaster) {
    expect(data).to.exist();
    expect(!data.type, 'invalid type: ' + data.type).to.be.true();
    /** @type string */
    var sn;
    /** @type number */
    var channel;
    // @ts-ignore value is not a string here
    forEach(data.value, (d) => {
      switch (d.index) {
      case ParamChannelMap.SN:
        sn = /** @type string */(d.value);
        assign(d, { type: XmlData.Type.STRING, unit: '', name: 'sn' });
        break;
      case ParamChannelMap.CHANNEL:
        d.value = channel = toNumber(d.value);
        assign(d, { type: XmlData.Type.UINT32, unit: '', name: 'channel' });
        break;
      default:
        if (isMaster) {
          this.parseChannel(d, false);
        }
        else {
          throw 'invalid parameter: ' + d.index;
        }
        break;
      }
    });
    if (this.daq) {
      // @ts-ignore sn and channel are assigned
      this.daq.checkChannelExist(sn, channel);
    }
    assign(data, { name: isMaster ? 'master' : 'slave' });
    return data;
  }

  /**
   * @param {Array<XmlParamPart>} masters
   * @returns {Array<XmlParamPart>}
   */
  parseConfiguration(masters) {
    return map(masters, (m) => this.parseChannel(m, true));
  }

  /**
   * @param {any} req
   */
  _handle(req) {
    if (!req || !req.xml) { throw 'invalid request: xml not found'; }

    var elt = xml.getChildNodes(req.xml, 'parameters')[0];
    if (!elt) { throw 'invalid request: missing parameters'; }

    var parameters = map(xml.getChildNodes(elt, 'data'), DisXmlParams.parseData);
    var mode;
    /** @type Array<XmlParamPart> */
    var configuration = [];

    forEach(parameters, (p) => {
      switch (p.index) {
      case ParamMap.MODE:
        mode = toNumber(p.value);
        break;
      case ParamMap.CONFIGURATION:
        // @ts-ignore not a string here
        configuration = this.parseConfiguration(p.value);
        break;
      }
    });
    if (isNil(mode)) {
      throw 'invalid request: mode parameter is required';
    }

    this.data[ParamMap.MODE].valueName =
      get(find(this.data[ParamMap.MODE].enum, { value: mode }), 'name', 'unknown');
    this.data[ParamMap.MODE].value = mode;
    this.data[ParamMap.CONFIGURATION].value = configuration;
    this._dirty = true;
    // @ts-ignore private
    this._updateXML();
  }
}

ZeroSuppression.Mode = Mode;

module.exports = ZeroSuppression;
