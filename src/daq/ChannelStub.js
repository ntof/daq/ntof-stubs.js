// @ts-check
const
  _ = require('lodash'),
  { DisXmlParams, DisXmlDataSet, XmlData } = require('@ntof/dim-xml');

class Channel extends DisXmlParams {
  /**
   * @param {number} index
   */
  constructor(index) {
    super();
    this.index = index;
    this.add({ name: "Number of parameters", index: 0, unit: '', type: XmlData.Type.INT32, value: 21 });
    this.add({ name: "Is channel enabled?", index: 1, unit: '', type: XmlData.Type.BOOL, value: 0 });
    this.add({ name: "Detector type", index: 2, unit: '', type: XmlData.Type.STRING, value: "BAF2" });
    this.add({ name: "Detector id", index: 3, unit: '', type: XmlData.Type.UINT32, value: 1 });
    this.add({ name: "Module type", index: 4, unit: '', type: XmlData.Type.STRING, value: "S412" });
    this.add({ name: "Channel id", index: 5, unit: '', type: XmlData.Type.UINT8, value: 0 });
    this.add({ name: "Module id", index: 6, unit: '', type: XmlData.Type.UINT8, value: 0 });
    this.add({ name: "Chassis id", index: 7, unit: '', type: XmlData.Type.UINT8, value: 119 });
    this.add({ name: "Stream id", index: 8, unit: '', type: XmlData.Type.UINT8, value: 1 });
    this.add({ name: "Sample rate", index: 9, unit: 'MS/s', type: XmlData.Type.FLOAT, value: 1800 });
    this.add({ name: "Sample size", index: 10, unit: 'kS', type: XmlData.Type.UINT32, value: 128000 });
    this.add({ name: "Full scale", index: 11, unit: 'mV', type: XmlData.Type.FLOAT, value: 5000 });
    this.add({ name: "Delay time", index: 12, unit: 'samples', type: XmlData.Type.INT32, value: 0 });
    this.add({ name: "Threshold", index: 13, unit: 'mV', type: XmlData.Type.FLOAT, value: 130 });
    this.add({ name: "Threshold sign", index: 14, unit: '', type: XmlData.Type.INT32, value: 1 });
    this.add({ name: "Zero suppression start", index: 15, unit: '', type: XmlData.Type.UINT32, value: 0 });
    this.add({ name: "Offset", index: 16, unit: 'mV', type: XmlData.Type.FLOAT, value: 0 });
    this.add({ name: "Pre samples", index: 17, unit: 'samples', type: XmlData.Type.UINT32, value: 512 });
    this.add({ name: "Post samples", index: 18, unit: 'samples', type: XmlData.Type.UINT32, value: 512 });
    this.add({ name: "Clock state", index: 19, unit: '', type: XmlData.Type.STRING, value: "INTC" });
    this.add({ name: "Input impedance", index: 20, unit: 'ohms', type: XmlData.Type.FLOAT, value: 50 });

    this.calibService = new DisXmlDataSet();
    this.calibService.add({ name: 'Calibrated full scale', index: 0, unit: 'mV', type: XmlData.Type.FLOAT, value: 0 });
    this.calibService.add({ name: 'Calibrated offset', index: 1, unit: 'mV', type: XmlData.Type.FLOAT, value: 0 });
    this.calibService.add({ name: 'Calibrated threshold', index: 2, unit: 'ADC', type: XmlData.Type.FLOAT, value: 0 });

    this.configured = false;
    this.updateCalibration();
  }

  /**
   * @param {any} value
   */
  setValue(value) {
    const initial = _.isNil(this.value);

    super.setValue(value);
    if (!initial) {
      this.setConfigured(true);
      this.emit('configured');
    }
  }

  /**
   * @param {any} req
   */
  _handle(req) {
    // @ts-ignore private
    super._handle(req);
    if (!this._dirty) {
      this._dirty = true;
      // @ts-ignore private
      _.defer(this._updateXML.bind(this));
    }

    this.updateCalibration();
  }

  /**
   * @returns {boolean}
   */
  isConfigured() {
    return this.configured;
  }

  /**
   * @param {boolean?} value
   */
  setConfigured(value) {
    this.configured = value ? true : false;
  }

  updateCalibration() {
    // What did you expect ?
    const fullScale = Number(this.get(11)) + 50;
    const threshold = fullScale ? Math.floor(Number(this.get(13)) * (1 << 12) / fullScale) : 0;
    this.calibService.update({ index: 0, value: fullScale });
    this.calibService.update({ index: 1, value: Number(this.get(16)) + 50 });
    this.calibService.update({ index: 2, value: threshold }); // S412 stub
  }
}

module.exports = Channel;
