// @ts-check
const { forEach, isEmpty, get, isNil } = require('lodash');

const
  { xml } = require('@ntof/dim-xml'),
  { DisService } = require('@cern/dim'),
  debug = require('debug')('stub:Addh'),
  _ = require('lodash');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

/** @enum {number} */
const DataSourceType = {
  SINGLE_DATA: 0,
  ALL_DATA: 1,
  VALUE: 2
};

class ExtraDataSource {
  /**
   * @param {DataSourceType} type
   * @param {string} fromService
   * @param {string?} fromName
   * @param {string?} destination
   * @param {string?} destinationPrefix
   */
  constructor(type, fromService, fromName = null, destination = null, destinationPrefix = null) {
    this.type = type;
    this.nodeName = type === DataSourceType.VALUE ? "value" : "data";
    this.fromService = fromService;
    this.destination = destination;
    this.destinationPrefix = destinationPrefix;
    this.fromName = fromName;
  }

  getObjForXmlSerialization() {
    const attributes = {
      fromService: this.fromService
    };

    switch (this.type) {
    case DataSourceType.SINGLE_DATA:
      _.assign(attributes, {
        fromName: this.fromName,
        destination: this.destination
      });
      break;
    case DataSourceType.ALL_DATA:
      _.assign(attributes, {
        destinationPrefix: this.destinationPrefix
      });
      break;
    case DataSourceType.VALUE:
      _.assign(attributes, {
        destination: this.destination
      });
      break;
    default:
      throw new Error("ExtraDataSource type not supported.");
    }

    return { '$': attributes };
  }
}

class AddhExtraService extends DisService {
  constructor() {
    super("C", '');

    /** @type Array<ExtraDataSource> */
    this.dataSources = [];
    // Create fake data sorurces
    this.dataSources.push(new ExtraDataSource(DataSourceType.SINGLE_DATA, 'Service_1', "ImportantData1", "DestData1", null));
    this.dataSources.push(new ExtraDataSource(DataSourceType.SINGLE_DATA, 'Service_1', "ImportantData2", "DestData2", null));
    this.dataSources.push(new ExtraDataSource(DataSourceType.SINGLE_DATA, 'Service_2', "ImportantData", "DestData", null));
    this.dataSources.push(new ExtraDataSource(DataSourceType.ALL_DATA, 'Service_All', null, null, "Data_"));
    this.dataSources.push(new ExtraDataSource(DataSourceType.ALL_DATA, 'Service_All2', null, null, "Data2_"));
    this.dataSources.push(new ExtraDataSource(DataSourceType.VALUE, 'Service_Basic1', null, 'DestBasic1', null));
    this.dataSources.push(new ExtraDataSource(DataSourceType.VALUE, 'Service_Basic2', null, 'DestBasic2', null));
    // Create xml and setValue
    this.updateXml();
  }

  updateXml() {
    const doc = xml.createDocument('extra');

    _.forEach(this.dataSources, (d) => {
      const node = doc.createElement(d.nodeName);
      xml.fromJs(node, d.getObjForXmlSerialization());
      doc.documentElement.appendChild(node);
    });
    this.setValue(xml.serializeToString(doc));
  }

  /**
   * @param {any} cmd
   */
  executeCommand(cmd) {
    // Clear extra sources
    this.dataSources = [];
    // Regenerate extra sources
    const dataSources = get(cmd, [ 'extra', 'data' ]);
    forEach(dataSources, (data) => {
      if (!isNil(get(data, '$')))
        data = get(data, '$');
      if (!isEmpty(data.destinationPrefix))
        this.dataSources.push(new ExtraDataSource(DataSourceType.ALL_DATA, data.fromService, null, null, data.destinationPrefix));
      else
        this.dataSources.push(new ExtraDataSource(DataSourceType.SINGLE_DATA, data.fromService, data.fromName, data.destination, null));
    });

    const valueSources = get(cmd, [ 'extra', 'value' ]);
    forEach(valueSources, (value) => {
      if (!isNil(get(value, '$')))
        value = get(value, '$');
      this.dataSources.push(new ExtraDataSource(DataSourceType.VALUE, value.fromService, null, value.destination, null));
    });
    // Update extra service xml value
    this.updateXml();
  }
}

class ADDHStub {
  constructor() {
    this.extraService = new AddhExtraService();
    // All Addh commands are: clear, reset, write, extra
    this.supportedCommands = [ 'extra' ];
  }


  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addService('ADDH/Extra', this.extraService);
    node.addXmlCmd('ADDH/Command', (req) => {
      const cmd = xml.toJs(req.xml);
      return this.command(_.get(cmd, 'command'));
    });
    debug('stub registered');
  }

  /**
   * @param {any} cmd
   * @returns {void}
   */
  command(cmd) {
    const command = _.get(cmd, "$.name", null);
    // Command sanity check
    if (!_.includes(this.supportedCommands, command)) { return; }

    if (command === 'extra') {
      this.extraService.executeCommand(cmd);
    }
  }

}

module.exports = ADDHStub;
