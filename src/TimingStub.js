// @ts-check
const
  { DisXmlParams, XmlData, xml } = require('@ntof/dim-xml'),
  { DisService } = require('@cern/dim'),
  debug = require('debug')('stub:Timing'),
  _ = require('lodash');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 */

const Mode = {
  DISABLED: 0,
  AUTOMATIC: 1,
  CALIBRATION: 2
};

const ParamIdx = {
  MODE: 1,
  REPEAT: 2,
  PERIOD: 3,
  PAUSE: 4,
  EVENT_NUMBER: 5
};

const OutputMode = {
  DISABLED: 0,
  ENABLED: 1
};

const TimingParamsMap = {
  mode: { index: 1, type: XmlData.Type.ENUM },
  triggerRepeat: { index: 2, type: XmlData.Type.INT32 },
  triggerPeriod: { index: 3, type: XmlData.Type.INT32 },
  triggerPause: { index: 4, type: XmlData.Type.INT32 },
  eventNumber: { index: 5, type: XmlData.Type.INT64 },
  calibOut: { index: 6, type: XmlData.Type.ENUM },
  parasiticOut: { index: 7, type: XmlData.Type.ENUM },
  primaryOut: { index: 8, type: XmlData.Type.ENUM }
};

class Timing extends DisXmlParams {
  constructor() {
    super();
    this._pauseTimerId = null;
    this._periodTimerId = null;
    this._repeat = 0;

    this.add({ name: "mode", index: ParamIdx.MODE, unit: "",
      type: XmlData.Type.ENUM, value: Mode.DISABLED, valueName: "DISABLED",
      enum: [
        { value: Mode.DISABLED, name: "DISABLED" },
        { value: Mode.AUTOMATIC, name: "AUTOMATIC" },
        { value: Mode.CALIBRATION, name: "CALIBRATION" }
      ]
    });
    this.add({ name: "Calibration trigger repeat", index: ParamIdx.REPEAT,
      unit: '', type: XmlData.Type.INT32, value: 1 });
    this.add({ name: "Calibration trigger period", index: ParamIdx.PERIOD,
      unit: 'ms', type: XmlData.Type.INT32, value: 2400 });
    this.add({ name: "Calibration trigger pause", index: ParamIdx.PAUSE,
      unit: 'ms', type: XmlData.Type.INT32, value: 0 });
    this.add({ name: "Event number", index: ParamIdx.EVENT_NUMBER,
      unit: '', type: XmlData.Type.INT64, value: 0 });

    this._eventService = new DisService('C', '');
    this.on('event', () => {
      // @ts-ignore
      this._eventService.setValue();
    });
  }

  close() {
    this._stopTimers();
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addXmlParams('Timing', this);
    node.addService('Timing/event', this._eventService);
  }

  /**
   * @param {any} req
   */
  _handle(req) {
    // @ts-ignore private
    super._handle(req);

    debug('timing cmd:', _.get(req, 'value'));
    this._stopTimers();
    if (this.get(ParamIdx.MODE) === Mode.CALIBRATION) {
      this._calibTick();
    }
  }

  _stopTimers() {
    if (!_.isNil(this._pauseTimerId)) {
      clearTimeout(this._pauseTimerId);
      this._pauseTimerId = null;
    }
    if (!_.isNil(this._periodTimerId)) {
      clearInterval(this._periodTimerId);
      this._periodTimerId = null;
    }
  }

  _calibTick() {
    debug('calibTick');
    if (!_.isNil(this._periodTimerId)) {
      this._repeat -= 1;
      this._emitEvent();

      if (this._repeat > 0) {
        return;
      }

      var pause = /** @type number */(this.get(ParamIdx.PAUSE));
      if (pause > 0) {
        clearInterval(this._periodTimerId);
        this._periodTimerId = null;
        this._pauseTimerId = setTimeout(this._calibTick.bind(this), pause);
        return;
      }
      this._repeat = /** @type number */(this.get(ParamIdx.REPEAT));
      return;
    }
    else {
      this._pauseTimerId = null;
      this._repeat = /** @type number */(this.get(ParamIdx.REPEAT));
      this._periodTimerId = setInterval(this._calibTick.bind(this),
        /** @type number */(this.get(ParamIdx.PERIOD)));
    }
  }

  /**
   * @param {any} event
   * @returns {string}
   */
  _eventToXml(event) {
    var doc = xml.createDocument('timing');
    var node = doc.createElement('event');
    xml.fromJs(node, { '$': event });
    doc.documentElement.appendChild(node);
    return xml.serializeToString(doc);
  }

  _emitEvent() {
    var eventNumber = /** @type number */(this.get(ParamIdx.EVENT_NUMBER)) + 1;
    var event;
    var now = _.now() * 1000000;
    if (this.get(ParamIdx.MODE) === Mode.CALIBRATION) {
      event = {
        name: 'CALIBRATION',
        dest: 'CALIB', dest2: '', user: '',
        timestamp: now, cyclestamp: now, periodNB: 1, eventNumber: eventNumber
      };
    }
    else {
      event = {
        name: 'PRIMARY',
        dest: '', dest2: '', user: '',
        timestamp: now, cyclestamp: now, periodNB: 1, eventNumber: eventNumber
      };
    }
    debug('event', event.name, event.eventNumber);
    this.emit('event', event);
    this._eventService.setValue(this._eventToXml(event));
    this.update(ParamIdx.EVENT_NUMBER, eventNumber);
  }
}
Timing.Mode = Mode;
Timing.ParamIdx = ParamIdx;
Timing.OutputMode = OutputMode;
Timing.Params = TimingParamsMap;

module.exports = Timing;
