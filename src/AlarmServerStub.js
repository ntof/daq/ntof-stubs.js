// @ts-check
const
  { xml } = require('@ntof/dim-xml'),
  { DisService } = require('@cern/dim'),
  debug = require('debug')('stub:AlarmServer'),
  _ = require('lodash'),
  { EventEmitter } = require('events');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 * @typedef {import('@ntof/dim-xml').xml.XmlJsObject} XmlJsObject
 * @typedef {{idx: number, str: string}} SeverytyMapValue
 * @typedef { "critical" | "error" | "warning" | "info" | "none" | "masked" | "disabled" } StatsKey
 */

/** @enum {SeverytyMapValue} */
const SeverityMap = {
  NONE: { idx: 0, str: "NONE" },
  INFO: { idx: 1, str: "INFO" },
  WARNING: { idx: 2, str: "WARNING" },
  ERROR: { idx: 3, str: "ERROR" },
  CRITICAL: { idx: 4, str: "CRITICAL" }
};

class Alarm {
  /**
   * @param {string} ident
    * @param {SeverytyMapValue} severity
    * @param {number?} date
    * @param {boolean} masked
    * @param {boolean} disabled
    */
  constructor(ident, severity = SeverityMap.NONE,  date = null,  masked = false, disabled = false) {
    this.ident = ident;
    this.severity = severity.idx;
    this.severityStr = severity.str;
    this.date = date; // lastActive
    this.active = true;
    this.masked = masked;
    this.disabled = disabled;
    this.system = 'system_' + ident;
    this.subsystem = 'subsystem_' + ident;
    this.source = 'source_' + ident;
    this.message = 'alarm message ' + ident;
  }

  /**
   * @return {XmlJsObject}
   */
  getObjForXmlSerialization() {
    return {
      '$': {
        ident: this.ident,
        severity: this.severity,
        severityStr: this.severityStr,
        date: this.date,
        system: this.system,
        subsystem: this.subsystem,
        source: this.source,
        masked: this.masked,
        disabled: this.disabled,
        active: this.active
      },
      '$textContent': this.message
    };
  }

}

class FakeAlarms extends EventEmitter {
  constructor() {
    super();
    /** @type Array<Alarm> */
    this.alarms = [];
    this.generateAlarms();
    this._timer = setInterval(this.fakeDynamicAlarmsChanges.bind(this), 2000);

  }

  release() {
    clearInterval(this._timer);
  }

  generateAlarms() {
    // Alarm1: Info.
    this.alarms.push(new Alarm('alarm1', SeverityMap.INFO, Date.now()));
    // Alarm2: None
    this.alarms.push(new Alarm('alarm2'));
    // Alarm3: Critical.
    this.alarms.push(new Alarm('alarm3', SeverityMap.CRITICAL, Date.now()));
    // Alarm4: Error.
    this.alarms.push(new Alarm('alarm4', SeverityMap.ERROR, Date.now()));
    // Alarm5: Warning.
    this.alarms.push(new Alarm('alarm5', SeverityMap.WARNING, Date.now()));
    // Alarm6: Critical but disabled.
    this.alarms.push(new Alarm('alarm6', SeverityMap.CRITICAL, Date.now(), false, true));
    // Alarm7: Error but masked.
    this.alarms.push(new Alarm('alarm7', SeverityMap.ERROR, Date.now(), true, false));
  }

  fakeDynamicAlarmsChanges() {
    const alarm = _.find(this.alarms, { ident: "alarm8" });
    if (alarm) {
      _.remove(this.alarms, (alarm) => alarm.ident === "alarm8");
    }
    else {
      this.alarms.push(new Alarm('alarm8', SeverityMap.CRITICAL, Date.now()));
    }
    this.emit('update', this.alarmsToXmlString());
  }

  /**
   * @returns {string}
   */
  alarmsToXmlString() {
    /** @type Record<StatsKey, number> */
    const stats = {
      "critical": 0, "error": 0, "warning": 0, "info": 0,
      "none": 0, "masked": 0, "disabled": 0
    };

    const doc = xml.createDocument('alarmserver');
    const alarmsNode = xml.createDocument('alarms');
    const statsNode = doc.createElement('stats');

    _.forEach(this.alarms, (alarm) => {
      const node = alarmsNode.createElement('alarm');
      if (!alarm.masked && !alarm.disabled) {
        stats[/** @type StatsKey */(alarm.severityStr.toLowerCase())]++;
      }
      if (alarm.masked) { stats['masked']++; }
      if (alarm.disabled) { stats['disabled']++; }
      xml.fromJs(node, alarm.getObjForXmlSerialization());
      alarmsNode.documentElement.appendChild(node);
    });

    xml.fromJs(statsNode, { "$": stats });

    doc.documentElement.appendChild(alarmsNode);
    doc.documentElement.appendChild(statsNode);

    return xml.serializeToString(doc);
  }
}


class AlarmServer extends DisService {
  constructor() {
    super("C", '');
    this.alarms = new FakeAlarms();
    this.setValue(this.alarms.alarmsToXmlString());

    this.alarms.on('update', (xml) => {
      this.setValue(xml);
    });
  }

  close() {
    this.alarms.release();
  }

  /**
   * @param {DisXmlNode} node
   */
  register(node) {
    node.addService('Alarms', this);
    node.addXmlCmd('Alarms', (req) => {
      const cmd = xml.toJs(req.xml);
      return this.command(_.get(cmd, 'command'));
    });
    debug('stub registered');
  }

  /**
   * @param {any} cmd
   */
  // eslint-disable-next-line complexity
  command(cmd) {
    const command = _.get(cmd, "$.name", null);
    const ident = _.get(cmd, "$.ident");
    // Command sanity check
    if (!command || (command !== "mask" && command !== "disable") || !ident) { return; }
    // Retrieve alarm
    const alarm = _.find(this.alarms.alarms, { ident: ident });
    if (!alarm) { debug(`Alarm ${ident} not found.`); return; }

    let update = false;
    if (command === "mask") {
      debug(`executing command mask for ${ident}. New masked value: ${!alarm.masked}`);
      alarm.masked = !alarm.masked;
      update = true;
    }
    if (command === "disable") {
      debug(`executing command disable for ${ident}. New disabled value: ${!alarm.disabled}`);
      alarm.disabled = !alarm.disabled;
      update = true;
    }

    if (update) {
      this.emit('update', this.alarms.alarmsToXmlString());
    }
  }

}

module.exports = AlarmServer;
