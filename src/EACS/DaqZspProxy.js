// @ts-check
const
  { DicXmlParams } = require('@ntof/dim-xml'),
  { makeDeferred } = require('@cern/prom'),
  ZeroSuppression = require('../daq/ZeroSuppression');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlCmd.ReqInfo} DicXmlReqInfo
 */

class DaqZspProxy extends ZeroSuppression {
  /**
   * @param {string} service
   * @param {string} dns
   */
  constructor(service, dns) {
    super();
    this.client = new DicXmlParams(service, null, dns);
    this.client.on('value', this.onValue.bind(this));
    this.isSyncing = false;
  }

  release() {
    this.client.release();
    this.emit('released');
  }

  /**
   * @param {boolean?} value
   */
  setSyncing(value) {
    this.isSyncing = (value !== false);
    if (this.client.value !== undefined) {
      this.onValue(this.client.value);
    }
  }

  /**
   * @param {number} timeout
   * @returns {Promise<void>}
   */
  async syncOnce(timeout = 3000) {
    const def = makeDeferred();
    const reject = () => def.reject('released');
    const resolve = () => def.resolve('synced');
    /** @typedef {NodeJS.Timeout | null} */
    var timer = setTimeout(() => {
      // @ts-ignore
      timer = null;
      def.reject('timeout');
    }, timeout);

    this.once('synced', resolve);
    this.once('released', reject);
    if (!this.isSyncing) { this.setSyncing(true); }
    return def.promise.finally(() => {
      clearTimeout(timer);
      this.removeListener('synced', resolve);
      this.removeListener('released', reject);
      this.setSyncing(false);
    });
  }

  /**
   * @returns {Promise<any>}
   */
  sendParameters() {
    return this.client.setParams(this.data);
  }

  /**
   * @param {any} value
   */
  onValue(value) {
    if (this.isSyncing) {
      this.data = value;
      this.emit('synced', value);
      this._dirty = true;
      // @ts-ignore private
      this._updateXML();
    }
  }
}

module.exports = DaqZspProxy;
