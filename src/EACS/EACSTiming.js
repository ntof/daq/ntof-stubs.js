// @ts-check
const
  { assign } = require('lodash'),
  DisXmlParamsProxy = require('./DisXmlParamsProxy'),
  TimingStub = require('../TimingStub');

class EACSTiming extends DisXmlParamsProxy {
  /**
   * @param {string} dns
   */
  constructor(dns) {
    super('Timing', dns);
  }

  /**
   * @returns {Promise<any>}
   */
  async suspend() {
    return this.client.setParams([
      assign({ value: TimingStub.Mode.DISABLED }, TimingStub.Params.mode),
      assign({ value: 0 }, TimingStub.Params.eventNumber)
    ]);
  }

  /**
   * @returns {Promise<any>}
   */
  async start() {
    this.update(assign({ value: 0 }, TimingStub.Params.eventNumber));
    return this.sendParameters();
  }
}

module.exports = EACSTiming;
