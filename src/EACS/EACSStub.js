// @ts-check
const
  debug = require('debug')('stub:eacs'),
  { get, forEach, noop } = require('lodash'),
  { xml, XmlCmd } = require('@ntof/dim-xml'),
  EACSState = require('./EACSState'),
  EACSRunInfo = require('./EACSRunInfo'),
  EACSRunConfig = require('./EACSRunConfig'),
  EACSEvents = require('./EACSEvents'),
  DaqList = require('./DaqList'),
  EACSTiming = require('./EACSTiming'),
  EACSInfo = require('./EACSInfo'),
  { delay } = require('@cern/prom');

const State = EACSState.State;
/**
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@ntof/dim-xml').DisXmlParams} DisXmlParams
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 * @typedef {{ name: string }} Cmd
 */

class EACS {
  /**
   * @param {Array<string>} daqList
   * @param {string} dns
   */
  constructor(daqList, dns) {
    this.daqs = new DaqList(daqList, dns);
    this.eacsInfo = new EACSInfo(dns);
    this.runInfo = new EACSRunInfo(this);
    this.runConfig = new EACSRunConfig(this);
    this.state = new EACSState();
    this.events = new EACSEvents(dns);
    this.timing = new EACSTiming(dns);

    this.state.setState(-1); /* NOT_READY */

    this._cmds = {
      start: this.start.bind(this),
      stop: this.stop.bind(this),
      reset: this.reset.bind(this)
    };
  }

  close() {
    this.events.close();
    this.daqs.close();
    this.timing.release();
  }

  /**
   * @param {string} name
   * @param {DisXmlNode} node
   */
  register(name, node) {
    node.addXmlState(name + '/State', this.state);
    node.addXmlDataSet(name + '/Info', this.eacsInfo);
    node.addXmlParams(name + '/RunInfo', this.runInfo);
    node.addXmlParams(name + '/RunConfig', this.runConfig);
    node.addXmlCmd(name, (req) => {
      const cmd = xml.toJs(req.xml);
      return this.command(get(cmd, 'command'));
    });
    node.addXmlDataSet(name + '/Events', this.events);
    this.daqs.register(name, node);
    node.addXmlParams(name + '/Timing', this.timing);

    this.reset().catch(noop);
  }

  /**
   * @param {Cmd} cmd
   */
  command(cmd) {
    // @ts-ignore it's ok
    const f = this._cmds[get(cmd, [ '$', 'name' ])];
    debug('command received:', f);
    if (f) {
      return f(cmd);
    }
    else {
      throw XmlCmd.createError(-1, 'unknown command');
    }
  }

  /**
   * @returns {number}
   */
  getState() {
    return this.state.states.value;
  }

  /**
   * @returns {boolean}
   */
  isActive() {
    switch (this.getState()) {
    case State.FILTERS_INIT:
    case State.SAMPLES_INIT:
    case State.COLLIMATOR_INIT:
    case State.DAQS_INIT:
    case State.STARTING:
    case State.RUNNING:
      return true;
    default: return false;
    }
  }

  async reset() {
    if (this.state.states.value >= 0 &&
        this.state.states.value !== State.IDLE &&
        this.state.states.value !== State.ERROR) {
      throw XmlCmd.createError(-1, 'invalid state for command: ' + this.getState());
    }
    forEach(this.state.states.errors, (e) => this.state.removeError(e.code));
    forEach(this.state.states.warnings, (e) => this.state.removeError(e.code));
    this.state.setState(State.LOADING);
    await this.daqs.reload();
    await this.timing.syncOnce();
    this.state.setState(State.IDLE);
  }

  async start() {
    if (this.getState() !== State.IDLE) {
      throw XmlCmd.createError(-1, 'invalid state for command: ' + this.getState());
    }

    this.runConfig.updateRunInfo(this.runInfo);
    this.runConfig.incRunNumber();

    if (!await this._startInit()) { return; }

    if (!await this._startDaq()) { return; }

    this.state.setState(State.STARTING);
    debug('starting');
    await this.timing.start();
    await this.timing.sendParameters();
    await delay(100);
    if (this.getState() !== State.STARTING) { return; }

    this.state.setState(State.RUNNING);
  }

  /**
   * @returns {Promise<boolean>}
   */
  async _startInit() {
    this.state.setState(State.FILTERS_INIT);
    await delay(200);
    if (this.getState() !== State.FILTERS_INIT) { return false; }

    this.state.setState(State.SAMPLES_INIT);
    await delay(200);
    if (this.getState() !== State.SAMPLES_INIT) { return false; }

    this.state.setState(State.COLLIMATOR_INIT);
    await delay(200);
    if (this.getState() !== State.COLLIMATOR_INIT) { return false; }

    this.state.setState(State.HIGHVOLTAGE_INIT);
    await delay(200);
    if (this.getState() !== State.HIGHVOLTAGE_INIT) { return false; }

    return true;
  }

  /**
   * @returns {Promise<boolean>}
   */
  async _startDaq() {
    this.state.setState(State.DAQS_INIT);
    await this.timing.suspend();
    if (this.getState() !== State.DAQS_INIT) { return false; }
    await delay(200);
    if (this.getState() !== State.DAQS_INIT) { return false; }

    return true;
  }

  async stop() {
    if (!this.isActive()) {
      throw XmlCmd.createError(-1, 'invalid state for command: ' + this.getState());
    }
    this.state.setState(State.IDLE);
  }

  config() {
    throw XmlCmd.createError(-1, 'not implemented (yet)');
  }
}

EACS.EACSState = EACSState;
EACS.EACSRunInfo = EACSRunInfo;
EACS.EACSRunConfig = EACSRunConfig;
EACS.EACSEvents = EACSEvents;

module.exports = EACS;
