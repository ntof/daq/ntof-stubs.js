// @ts-check
const
  { filter } = require('lodash'),
  { DicXmlParams, DisXmlParams } = require('@ntof/dim-xml'),
  { makeDeferred } = require('@cern/prom');

class DisXmlParamsProxy extends DisXmlParams {
  /**
   * @param {string} service
   * @param {string} dns
   */
  constructor(service, dns) {
    super();
    this.client = new DicXmlParams(service, null, dns);
    this.client.on('value', this.onValue.bind(this));
    this.isSyncing = false;
    this.readOnly = new Set();
  }

  release() {
    this.client.release();
    this.emit('released');
  }

  /**
   * @param {boolean?} value
   */
  setSyncing(value) {
    this.isSyncing = (value !== false);
    if (this.client.value !== undefined) {
      this.onValue(this.client.value);
    }
  }

  /**
   * @param {number} index
   * @param {boolean?} readOnly
   */
  setReadOnly(index, readOnly) {
    if (readOnly === false) {
      this.readOnly.delete(index);
    }
    else {
      this.readOnly.add(index);
    }
  }

  /**
   * @param {number} timeout
   * @returns {Promise<void>}
   */
  async syncOnce(timeout = 3000) {
    const def = makeDeferred();
    const reject = () => def.reject('released');
    const resolve = () => def.resolve('synced');
    var timer = setTimeout(() => {
      // @ts-ignore
      timer = null;
      def.reject('timeout');
    }, timeout);

    this.once('synced', resolve);
    this.once('released', reject);
    if (!this.isSyncing) { this.setSyncing(true); }
    return def.promise.finally(() => {
      clearTimeout(timer);
      this.removeListener('synced', resolve);
      this.removeListener('released', reject);
      this.setSyncing(false);
    });
  }

  /**
   * @returns {Promise<any>}
   */
  sendParameters() {
    return this.client.setParams(
      filter(this.data, (d) => !this.readOnly.has(d.index)));
  }

  /**
   * @param {any} value
   */
  onValue(value) {
    if (this.isSyncing) {
      this.data = value;
      this.emit('synced', value);
      this._dirty = true;
      // @ts-ignore private
      this._updateXML();
    }
  }
}

module.exports = DisXmlParamsProxy;
