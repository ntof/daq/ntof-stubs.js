// @ts-check
const
  { get } = require('lodash'),
  { DisXmlParams } = require('@ntof/dim-xml'),
  { Params } = require('./EACSRunInfo'),
  { makeParam } = require('./utils');

/**
 * @typedef {import('./EACSStub')} Eacs
 * @typedef {import('./EACSRunInfo')} EACSRunInfo
 */

const ConfigParams = [
  Params.runNumber,
  Params.title,
  Params.description,
  Params.experiment,
  Params.detectorsSetupId, Params.materialsSetupId
];

class EACSRunConfig extends DisXmlParams {
  /**
   * @param {Eacs} eacs
   */
  constructor(eacs) {
    super();
    this.eacs = eacs;

    for (const p of ConfigParams) {
      this.add(makeParam(p));
    }

    this.incRunNumber();
  }

  incRunNumber() {
    const value = /** @type number */(this.get(Params.runNumber.index)) + 1;
    this.update({ index: Params.runNumber.index, value });
  }

  /**
   * @param {EACSRunInfo} info
   */
  updateRunInfo(info) {
    for (const p of ConfigParams) {
      // @ts-ignore not undefined
      info.update({ value: this.get(p.index), index: p.index });
    }
  }

  /**
   * @param {any} data
   * @returns {boolean}
   */
  checkParam(data) {
    if (!super.checkParam(data)) {
      return false;
    }

    const index = get(data, 'index');
    switch (index) {
    default:
      return true;
    case Params.runNumber.index:
      return false;
    }
  }
}

EACSRunConfig.Params = Params;

module.exports = EACSRunConfig;
