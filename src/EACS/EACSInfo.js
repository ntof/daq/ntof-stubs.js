// @ts-check
const
  { find, isNil, times } = require('lodash'),
  { DisXmlDataSet, XmlData } = require('@ntof/dim-xml');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 * @typedef {import('@ntof/dim-xml').DicXmlValue} DicXmlValue
 * @typedef {import('@ntof/dim-xml').DisXmlDataSet.UpdatePart} UpdatePart
 * @typedef {import('@ntof/dim-xml').XmlDataSet} XmlDataSet
 */

class EACSInfo extends DisXmlDataSet {
  /**
   * @param {string} dns
   */
  constructor(dns) {
    super();
    this.dns = dns;
  }

  /**
   * @param {string} path
   */
  addFilterStation(path) {
    if (isNil(this.get(0))) {
      this.add({ index: 0, name: "filterStation",
        type: XmlData.Type.STRING, value: path
      });
    }
    else {
      this.update(/** @type UpdatePart */({ index: 0, name: "filterStation",
        type: XmlData.Type.STRING, value: path
      }));
    }
  }

  /**
   * @param {number} crateIndex
   * @param {number} nChannels
   */
  addHighVoltage(crateIndex, nChannels) {
    if (isNil(this.get(1))) {
      this.add({ index: 1, name: "highVoltage", value: [] });
    }

    /**
     * @param {XmlData} crate
     */
    const pushChannelIntoCrate = (crate) => {
      crate.value = [];
      let channelIndex = 0;
      times(nChannels, () => crate.value.push({ index: channelIndex++, name: 'channel' }));
    };

    var crates = /** @type XmlDataSet */(this.getNested(1));
    const crate = find(crates, { index: crateIndex });
    if (crate) {
      pushChannelIntoCrate(crate);
    }
    else {
      const newCrate = { index: crateIndex, name: 'HV_' + crateIndex, value: [] };
      pushChannelIntoCrate(newCrate);
      crates.push(newCrate);
    }
    this._dirty = true;
    // @ts-ignore private
    this._updateXML();
  }
}

module.exports = EACSInfo;
