// @ts-check
const
  { get } = require('lodash'),
  { DisXmlDataSet, XmlData, DicXmlValue, xml } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:eacs');

/**
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@ntof/dim-xml').XmlDataSet} XmlDataSet
 *
 * @typedef {{
 *   name: string,
 *   timestamp: number,
 *   cyclestamp: number,
 *   periodNB: number,
 *   dest: string,
 *   dest2: string,
 *   user: string,
 *   eventNumber: number
 * }} Event
 */

const MAX_EVENTS = 10;

class EACSEvents extends DisXmlDataSet {
  /*::
  client: DicXmlValue
  */
  /**
   * @param {DnsClient | string} dns
   */
  constructor(dns) {
    super();
    this.client = new DicXmlValue('Timing/event', { stamped: true }, dns);
    this.client.on('value', this.onValue.bind(this));
  }

  close() {
    this.client.release();
  }

  /**
   * @param {any} value
   */
  onValue(value) {
    if (value) {
      this.addEvent(get(xml.toJs(value), 'event.$'));
    }
  }

  /**
   * @param {Event} event
   */
  addEvent(event) {
    debug('event', event.eventNumber);
    this.add({ index: event.eventNumber, name: 'event',
      value: [
        { index: 0, name: 'eventNumber', type: XmlData.Type.UINT32, value: event.eventNumber },
        // no validated number until validated
        { index: 2, name: 'beamType', type: XmlData.Type.STRING, value: event.name },
        // no protons until validated
        { index: 4, name: 'cyclestamp', type: XmlData.Type.INT64, value: event.cyclestamp },
        { index: 5, name: 'periodNB', type: XmlData.Type.INT32, value: event.periodNB }
      ]
    });
    if (this.data.length > MAX_EVENTS) {
      // @ts-ignore it is ok
      this.data = this.data.splice(this.data.length - MAX_EVENTS);
    }
  }
}

module.exports = EACSEvents;
