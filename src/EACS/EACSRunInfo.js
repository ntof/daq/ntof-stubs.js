// @ts-check
const
  { get, forEach } = require('lodash'),
  { DisXmlParams, XmlData } = require('@ntof/dim-xml'),
  { State } = require('./EACSState');

/**
 * @typedef {import('./EACSStub')} Eacs
 * @typedef {{ index: number, type: number, default: any }} XmlDataDesc
 */

/** @type {{ [id: string]: XmlDataDesc }} */
const RunInfoMap = {
  runNumber: { index: 1, type: XmlData.Type.UINT32, default: 920001 },
  eventNumber: { index: 2, type: XmlData.Type.UINT32, default: 0 },
  validatedNumber: { index: 3, type: XmlData.Type.UINT32, default: 0 },
  validatedProtons: { index: 4, type: XmlData.Type.DOUBLE, default: 0 },
  title: { index: 5, type: XmlData.Type.STRING, default: 'myExperiment' },
  description: {
    index: 6, type: XmlData.Type.STRING, default: `
    # This is a title

    Some content`
  },
  area: { index: 7, type: XmlData.Type.STRING, default: 'LAB' },
  experiment: { index: 8, type: XmlData.Type.STRING, default: 'test' },
  detectorsSetupId: { index: 9, type: XmlData.Type.INT64, default: -1 },
  materialsSetupId: { index: 10, type: XmlData.Type.INT64, default: -1 }
};

class EACSRunInfo extends DisXmlParams {
  /*::
  eacs: EACS

  static Params: typeof RunInfoMap
  */
  /**
   * @param {Eacs} eacs
   */
  constructor(eacs) {
    super();
    this.eacs = eacs;

    forEach(RunInfoMap, (value, key) => this.add({
      index: value.index, name: key, type: value.type, value: value.default
    }));
  }

  /**
   * @param {any} data
   * @returns {boolean}
   */
  checkParam(data) {
    if (!super.checkParam(data)) {
      return false;
    }

    const index = get(data, 'index');
    switch (index) {
    case RunInfoMap.title.index:
    case RunInfoMap.description.index:
      return true;
    case RunInfoMap.experiment.index:
    case RunInfoMap.detectorsSetupId.index:
    case RunInfoMap.materialsSetupId.index:
      return this.eacs.state.states.value === State.IDLE;
    default:
      return false;
    }
  }
}

EACSRunInfo.Params = RunInfoMap;

module.exports = EACSRunInfo;
