// @ts-check
const
  { timeout } = require('@cern/prom'),
  { forEach, invoke, get, toUpper, map, transform } = require('lodash'),
  { DicXmlValue, xml } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:eacs'),
  EventEmitter = require('events'),
  DisXmlParamsProxy = require('./DisXmlParamsProxy'),
  DaqZspProxy = require('./DaqZspProxy');

class Daq extends EventEmitter {
  /**
   * @param {string} hostname
   * @param {string} dns
   */
  constructor(hostname, dns) {
    super();
    this.hostname = hostname;
    this.dns = dns;
    /** @type {{ [id: string]: DisXmlParamsProxy }} */
    this.channels = {};
    this.zsp = new DaqZspProxy(this.hostname + '/ZeroSuppression', dns);
  }

  close() {
    forEach(this.channels, (c) => c.release());
    this.zsp.release();
    invoke(this._client, 'release');
  }

  async load() {
    if (this.isLoading) { return; }

    try {
      this._setLoading(true);

      this._client = new DicXmlValue(this.hostname + '/ListDaqElements', null, this.dns);
      const prom = timeout(this._client.promise(), 3000);
      forEach(this.channels, (c) => c.release());
      this.channels = {};
      const daqs = transform(xml.toJs(/** @type Element */(await prom)),
        (/** @type {{ [id: string]: any }} */ret, card, key) => {
          if (key.startsWith('card')) {
            ret[key] = card['$'];
          }
        }, {});
      forEach(daqs, (d, name) => {
        for (let i = 0; i < get(d, 'nbChannel', 0); ++i) {
          const path = `${this.hostname}/${toUpper(name)}/CHANNEL${i}`;
          this.channels[path] = new DisXmlParamsProxy(path, this.dns);
        }
      });
      this.emit('listDaqElements', daqs);
      await Promise.all(map(this.channels, (chan) => chan.syncOnce()));
      debug('daq config loaded:', this.hostname);
      await this.zsp.syncOnce();
      debug('zsp config loaded:', this.hostname);
    }
    finally {
      invoke(this._client, 'release');
      this._setLoading(false);
    }
  }

  /**
   * @param {boolean} value
   */
  _setLoading(value) {
    this.isLoading = value;
    this.emit('isLoading', value);
  }
}

module.exports = Daq;
