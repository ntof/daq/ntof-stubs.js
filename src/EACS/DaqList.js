// @ts-check
const
  { forEach, map, toNumber, every, invoke, isEmpty } = require('lodash'),
  { DisXmlDataSet, XmlData } = require('@ntof/dim-xml'),
  { makeDeferred } = require('@cern/prom'),
  Daq = require('./Daq'),
  debug = require('debug')('stub:eacs');

/**
 * @typedef {import('@ntof/dim-xml').DisXmlNode} DisXmlNode
 * @typedef {import('@ntof/dim-xml').DicXmlValue} DicXmlValue
 * @typedef {{
 *   index: number,
 *   name: string,
 *   type: number,
 *   value: Array<{ index: number, name: string, type: number, value: any }>,
 * }} CardData
 */

class DaqList extends DisXmlDataSet {
  /**
   * @param {Array<string>} daqs
   * @param {string} dns
   */
  constructor(daqs, dns) {
    super();
    this.dns = dns;
    /** @type {{ [id: string]: Daq }} */
    this.daqs = {};
    /** @type {{ [id: string]: any }} */
    this.info = {};
    forEach(daqs, (d) => {
      this.addDaq(d);
    });
  }

  close() {
    forEach(this.daqs, (d) => d.close());
  }

  /**
   * @param {string} name
   * @param {DisXmlNode} node
   */
  register(name, node) {
    this.name = name;
    this.node = node;
    node.addXmlDataSet(name + '/Daq/List', this);
  }

  async load() {
    if (!this.node) {
      console.warn('Please register the EACS on a DNS first');
      return;
    }
    else if (isEmpty(this.daqs)) {
      /* nothing to load */
      return;
    }

    if (!this._loader) {
      this._loader = makeDeferred();
      this.isLoading = true;
      this.emit('isLoading', true);
      await Promise.all(map(this.daqs, (d) => d.load()));
    }
    await this._loader.promise;
  }

  /**
   * @returns {Promise<void>}
   */
  async reload() {
    if (this._loader) {
      this._loader.reject('reload');
      this._loader = null;
    }
    return this.load();
  }

  /**
   * @param {number} index
   * @param {any} info
   * @returns {CardData}
   */
  _makeCardData(index, info) {
    return {
      index, name: "card", type: XmlData.Type.NESTED,
      value: [
        { index: 0, name: 'channelsCount', type: XmlData.Type.UINT32, value: info.nbChannel },
        { index: 1, name: 'type', type: XmlData.Type.STRING, value: info.type },
        { index: 2, name: 'serialNumber', type: XmlData.Type.STRING, value: info.serialNumber },
        { index: 3, name: 'used', type: XmlData.Type.BOOL, value: '0' }
      ]
    };
  }

  /**
   * @param {string} hostname
   */
  addDaq(hostname) {
    if (this.daqs[hostname]) {
      debug('Daq already registered:', hostname);
      return;
    }
    const daq = new Daq(hostname, this.dns);
    daq.on('listDaqElements', this.onDaqElements.bind(this, hostname));
    daq.on('isLoading', this.onDaqLoading.bind(this));
    this.daqs[hostname] = daq;

    this.info[hostname] = {
      name: 'daq',
      type: XmlData.Type.NESTED,
      value: [
        { index: 0, name: "name", type: XmlData.Type.STRING, value: hostname },
        {
          index: 1, name: "cards", type: XmlData.Type.NESTED,
          value: []
        },
        { index: 2, name: 'used', type: XmlData.Type.BOOL, value: '0' }
      ]
    };
    this.add(this.info[hostname]);
  }

  /**
   * @param {string} hostname
   * @param {any} daqs
   */
  onDaqElements(hostname, daqs) {
    const daq = this.daqs[hostname];
    if (!this.node) return;
    forEach(daq.channels, (chan, path) => {
      // @ts-ignore this.node is checked above. You little blind tool!
      this.node.addXmlParams(this.name + '/Daq/' + path, chan);
    });
    this.node.addXmlParams(this.name + '/Daq/' + hostname + '/ZeroSuppression',
      daq.zsp);

    this.info[hostname].value[1].value = map(daqs,
      (info, name) => this._makeCardData(toNumber(name.substring(4)), info));
    this._dirty = true;
    // @ts-ignore private
    this._updateXML();
  }

  /**
   * @param {boolean} state
   */
  onDaqLoading(state) {
    if (state === true) { return; } /* don't care about this */

    if (every(this.daqs, (d) => !d.isLoading)) {
      invoke(this._loader, 'resolve');
      this.isLoading = false;
      this.emit('isLoading', false);
      debug('all daqs loaded');
    }
  }
}

module.exports = DaqList;
