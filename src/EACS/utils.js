// @ts-check

/**
 * @typedef {{ index: number, type: number, default: any }} XmlDataDesc
 */

/**
 * @param {XmlDataDesc} info
 * @returns {any}
 */
function makeParam(info) {
  return { index: info.index, type: info.type, value: info.default };
}

module.exports = { makeParam };
