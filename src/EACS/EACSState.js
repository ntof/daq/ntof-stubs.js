// @ts-check
const
  { DisXmlState } = require('@ntof/dim-xml'),
  debug = require('debug')('stub:eacs');

const State = {
  LOADING: 1,
  IDLE: 2,
  FILTERS_INIT: 3,
  SAMPLES_INIT: 4,
  COLLIMATOR_INIT: 5,
  HIGHVOLTAGE_INIT: 6,
  DAQS_INIT: 7,
  MERGER_INIT: 8,
  STARTING: 9,
  RUNNING: 10,
  STOPPING: 11,
  ERROR: -2
};

class EACSState extends DisXmlState {
  constructor() {
    super();
    this.stateChanges = 0;
    this.addState(State.LOADING, "LOADING");
    this.addState(State.IDLE, "IDLE");
    this.addState(State.FILTERS_INIT, "FILTERS INITIALISATION");
    this.addState(State.SAMPLES_INIT, "SAMPLES INITIALISATION");
    this.addState(State.COLLIMATOR_INIT, "COLLIMATOR INITIALISATION");
    this.addState(State.HIGHVOLTAGE_INIT, "HIGHVOLTAGE INITIALISATION");
    this.addState(State.DAQS_INIT, "DAQS INITIALISATION");
    this.addState(State.MERGER_INIT, "MERGER INITIALISATION");
    this.addState(State.STARTING, "STARTING");
    this.addState(State.RUNNING, "RUNNING");
    this.addState(State.ERROR, "ERROR");
  }

  /**
   * @param {number} value
   * @returns {boolean}
   */
  setState(value) {
    ++this.stateChanges;
    debug('state change:', value);
    return super.setState(value);
  }
}

EACSState.State = State;

module.exports = EACSState;
