#!/usr/bin/env node

// @ts-check
const
  cmd = require('commander'),
  { map, toNumber } = require('lodash'),
  { NTOFStub } = require('../src'),
  debug = require('debug')('stub:script'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

/* enable dirty-chai */
chai.use(dirtyChai);

function collectDaq(value, ret) {
  ret = ret || [];
  ret.push(map(value.split(':'), toNumber));
  return ret;
}

function collectHv(value, ret) {
  ret = ret || [];
  ret.push(toNumber(value));
  return ret;
}

cmd
.option('-d, --debug', 'Enable debug')
.option('-p, --port <item>', 'Provide dns port', '2505')
.option('--daq <channels>',
  'Add a daq (ex: --daq 2:4, 2 cards 2 and 4 channels each)', collectDaq, null)
.option('--hv <channels>',
  'Add a hv (ex: --hv 2, adds a card with 2 channels)', collectHv, null)
.option('--vgr <number>',
  'Add Vacuum Value services (ex: --vgr 2, adds 2 vacuum values services)', '2')
.option('--vvs <number>',
  'Add Value Position services (ex: --vvs 2, adds 2 valve position services)', '2')
.option('--vpp <number>',
  'Add Vacuum Pump services (ex: --vpp 2, adds 2 vacuum pump services)', '2');

cmd.parse(process.argv);
var opts = cmd.opts();

(async () => {
  const config = {
    port: toNumber(opts.port),
    daqs: map(opts.daq || [ [ 2 ] ],
      (layout, idx) => ({ crateId: idx, layout })),
    hv: map(opts.hv || [ 2 ],
      (nChannels, idx) => ({ cardId: idx, nChannels })),
    beamline: {
      vgr: toNumber(opts.vgr),
      vvs: toNumber(opts.vvs),
      vpp: toNumber(opts.vpp)
    }
  };
  debug('Using config:', JSON.stringify(config));

  // @ts-ignore it's ok
  const stub = new NTOFStub(config);
  await stub.init();
})();
